


# Commodore 64 Programmer´s Reference Guide

 ![Commodore 64 Programmer's Reference Guide](cover.jpg)

 This markdown version of the original Commodore Busines Machines´
 Commodore 64 Programmer´s Reference Guide is based on the 1996 etext
 version by Ville Muikkula as found on the Project 64 web page or on
 other places of the internet.

 I tried to stick as close to the original content as possible,
 however I am not striving to make as close a reproduction of the
 original layout as possible but rather have well formated readable
 text that uses a consistend typographic philosophy. Especially the
 exessive `yelling' of the original was sacrificed in favour of better
 readability. In the epub-version this will be resurrected to a
 certain extent by means of CSS styles.

 The typeface of the original print version looks like "Futura" and
 "Futura Bold" by Paul Renner.