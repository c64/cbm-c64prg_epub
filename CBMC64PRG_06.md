 <span epub:type="pagebreak" id="p335" class="pagenumber">335</span>
 <a name="top">



# CHAPTER 6 — Input/Output Guide


 * [ Introduction ](#06-1)
 * [ Output to the TV ](#06-2)
 * [ Output to Other Devices ](#06-3)
 * [ The Game Ports ](#06-4)
 * [ RS-232 Interface Description ](#06-5)
 * [ The User Port ](#06-6)
 * [ The Serial Bus ](#06-7)
 * [ The Expansion Port ](#06-8)
 * [ Z-80 Microprocessor Cartridge ](#06-9)


 <span epub:type="pagebreak" id="p336" class="pagenumber">336</span>
 <a name="06-1">



## Introduction

 Computers have three basic abilities: they can calculate, make
 decisions, and communicate. Calculation is probably the easiest to
 program.  Most of the rules of mathematics are familiar to
 us. Decision making is not too difficult, since the rules of logic
 are relatively few, even if you don't know them too well yet.

 Communication is the most complex, because it involves the least
 exacting set of rules. This is not an oversight in the design of
 computers. The rules allow enough flexibility to communicate
 virtually anything, and in many possible ways. The only real rule is
 this: whatever sends information must present the information so that
 it can be understood by the receiver.
 <a name="06-2">





## Output to the TV

 The simplest form of output in BASIC is the PRINT statement. PRINT
 uses the TV screen as the output device, and your eyes are the input
 device because they use the information on the screen.

 When PRINTing on the screen, your main objective is to format the
 information on the screen so it's easy to read. You should try to
 think like a graphic artist, using colors, placement of letters,
 capital and lower case letters, as well as graphics to best
 communicate the information. Remember, no matter how smart your
 program, you want to be able to understand what the results mean to
 you.

 The PRINT statement uses certain character codes as “commands” to the
 cursor. The <kbd>CRSR</kbd> key doesn't actually display anything, it just
 makes the cursor change position. Other commands change colors, clear
 the screen, and insert or delete spaces. The <kbd>RETURN</kbd> key has a
 character code number (CHR$) of 13. A complete table of these codes
 is contained in Appendix C.

 There are two functions in the BASIC language that work with the
 PRINT statement. TAB positions the,cursor on the given position from
 the left edge of the screen, SPC moves the cursor right a given
 number of spaces from the current position.

 Punctuation marks in the PRINT statement serve to separate and format
 information. The semicolon (;) separates 2 items without any spaces
 in between. If it is the last thing on a line, the cursor remains
 after the last thing PRINTed instead of going down to the next
 line. It suppresses
 <span epub:type="pagebreak" id="p337" class="pagenumber">337</span>
 (replaces) the RETURN character that is normally PRINTed at the end
 of the line.

 The comma (,) separates items into columns. The Commodore 64 has 4
 columns of 10 characters each on the screen. When the computer PRINTs
 a comma, it moves the cursor right to the start of the next
 column. If it is past the last column of the line, it moves the
 cursor down to the next line. Like the semicolon, if it is the last
 item on a line the RETURN is suppressed.

 The quote marks ("") separate literal text from variables. The first
 quote mark on the line starts the literal area, and the next quote
 mark ends it. By the way, you don't have to have a final quote mark
 at the end of the line.

 The RETURN code (CHR$ code of 13) makes the cursor go to the next
 logical line on the screen. This is not always the very next
 line. When you type past the end of a line, that line is linked to
 the next line.  The computer knows that both lines are really one
 long line. The links are held in the line link table (see the memory
 map for how this is set up).

 A logical line can be 1 or 2 screen lines long, depending on what was
 typed or PRINTed.  The logical line the cursor is on determines where
 the <kbd>RETURN</kbd> key sends it. The logical line at the top of
 the screen determines if the screen scrolls 1 or 2 lines at a
 time. There are other ways to use the TV as an output device. The
 chapter on graphics describes the commands to create objects that
 move across the screen. The VIC chip section tells how the screen and
 border colors and sizes are changed. And the sound chapter tells how
 the TV speaker creates music and special effects.
 <a name="06-3">




## Output to Other Devices

 It is often necessary to send output to devices other than the
 screen, like a cassette deck, printer, disk drive, or modem. The OPEN
 statement in BASIC creates a “channel” to talk to one of these
 devices. Once the channel is OPEN, the PRINT# statement will send
 characters to that device.

 **Example** of OPEN and PRINT# Statements:

``` basic
100 OPEN 4,4: PRINT# 4, "WRITING ON PRINTER"
110 OPEN 3,8,3,"0:DISK-FILE,S,W":PRINT#3,"SEND TO DISK"
120 OPEN 1,1,1,"TAPE-FILE": PRINT#1,"WRITE ON TAPE"
130 OPEN 2,2,0,CHR$(10):PRINT#2,"SEND TO MODEM"
```

<span epub:type="pagebreak" id="p338" class="pagenumber">338</span>


 The OPEN statement is somewhat different for each device. The
 parameters in the OPEN statement are shown in the table below for
 each device.

 TABLE of OPEN Statement Parameters:

 FORMAT: OPEN file#, device#, number, string

 | Device | Device# |       Number           |          String          |
 |:------ |:-------:|:---------------------- |:------------------------ |
 |CASSETTE|    1    | 0 = Input              | File Name                |
 |        |         | 1 = Output             |                          |
 |        |         | 2 = Output with EOT    |                          |
 | MODEM  |    2    | 0                      | Control Registers        |
 | SCREEN |    3    | 0,1                    |                          |
 | PRINTER|  4 or 5 | 0 = Upper/Graphics     | Text Is PRINTed          |
 |        |         | 7 = Upper/Lower Case   |                          |
 | DISK   | 8 to 11 | 2-14 = Data Channel    | Drive #, File Name File Type, Read/Write Command |
 |        |         | 15 = Command Channel   |                          |




### Output to Printer

 The printer is an output device similar to the screen. Your main
 concern when sending output to the printer is to create a format that
 is easy on the eyes. Your tools here include reversed, double-width,
 capital and lower case letters, as well as dot-programmable graphics.

 The SPC function works for the printer in the same way it works for
 the screen. However, the TAB function does not work correctly on the
 printer, because it calculates the current position on the line based
 on the cursor's position on the screen, not on the paper.

 The OPEN statement for the printer creates the channel for
 communication. It also specifies which character set will be used,
 either upper case with graphics or upper and lower case.

 **Examples** of OPEN Statement for Printer:

``` c64
OPEN 1,4: REM UPPER CASE/GRAPHICS
OPEN 1,4,7: REM UPPER AND LOWER CASE
```
<span epub:type="pagebreak" id="p339" class="pagenumber">339</span>

 When working with one character set, individual lines can be PRINTed
 in the opposite character set. When in upper case with graphics, the
 cursor down character (CHR$(17)) switches the characters to the upper
 and lower case set. When in upper and lower case, the cursor up
 character (CHR$(145)) allows upper case and graphics characters to be
 PRINTed.

 Other special functions in the printer are controlled through
 character codes. All these codes are simply PRINTed just like any
 other character.

 TABLE of Printer Control Character Codes:

 | CHR$ Code |                        Purpose                            |
 | ---------:|:--------------------------------------------------------- |
 |     10    |  Line feed                                                |
 |     13    |  RETURN (automatic line feed on CBM printers)             |
 |     14    |  Begin double-width character mode                        |
 |     15    |  End double-width character mode                          |
 |     18    |  Begin reverse character mode                             |
 |    146    |  End reverse character mode                               |
 |     17    |  Switch to upper/lower case character set                 |
 |    145    |  Switch to upper case/graphics character set              |
 |     16    |  Tab to position in next 2 characters                     |
 |     27    |  Move to specified dot position                           |
 |      8    |  Begin dot-programmable graphic mode                      |
 |     26    |  Repeat graphics data                                     |

 See your Commodore printer's manual for details on using the command
 codes.




### Output to Modem

 The modem is a simple device that can translate character codes into
 audio pulses and viceversa, so that computers can communicate over
 telephone lines. The OPEN statement for the modem sets up the
 parameters to match the speed and format of the other computer you
 are communicating with. Two characters can be sent in the string at
 the end of the OPEN statement.

 The bit positions of the first character code determine the baud
 rate, number of data bits, and number of stop bits. The second code
 is optional, and its bits specify the parity and duplex of the
 transmission.  See the RS-232 section or your VICMODEM manual for
 specific details on this device.
 <span epub:type="pagebreak" id="p340" class="pagenumber">340</span>



 **Example** of OPEN Statement for Modem:

``` c64
OPEN 1,2,0,CHR$(6): REM 300 BAUD
100 OPEN 2,2,0,CHR$(163) CHR$(112): REM 110 BAUD, ETC.
```

 Most computers use the American Standard Code for Information
 Interchange, known as ASCII (pronounced ASK-KEY). This standard set
 of character codes is somewhat different from the codes used in the
 Commodore 64. When communicating with other computers, the Commodore
 character codes must be translated into their ASCII counterparts. A
 table of standard ASCII codes is included in this book in Appendix C.

 Output to the modem is a fairly uncomplicated task, aside from the
 need for character translation. However, you must know the receiving
 device fairly well, especially when writing programs where your
 computer “talks” to another computer without human intervention. An
 example of this would be a terminal program that automatically types
 in your account number and secret password. To do this successfully,
 you must carefully count the number of characters and RETURN
 characters. Otherwise, the computer receiving the characters won't
 know what to do with them.




### Working with Cassette Tape

 Cassette tapes have an almost unlimited capacity for data. The longer
 the tape, the more information it can store. However, tapes are
 limited in time. The more data on the tape, the longer the time it
 takes to find the information.

 The programmer must try to minimize the time factor when working with
 tape storage. One common practice is to read the entire cassette data
 file into RAM, then process it, and then re-write all the data on the
 tape. This allows you to sort, edit, and examine your data. However,
 this limits the size of your files to the amount of available RAM.

 If your data file is larger than the available RAM, it is probably
 time to switch to using the floppy disk. The disk can read data at
 any position on the disk, without needing to read through all the
 other data.  You can write data over old data without disturbing the
 rest of the file.  That's why the disk is used for all business
 applications like ledgers and mailing lists.

 The PRINT# statement formats data just like the PRINT statement does.
 All punctuation works the same. But remember, you're not working with
 the screen now. The formatting must be done with the INPUT# statement
 constantly in mind.
 <span epub:type="pagebreak" id="p341" class="pagenumber">341</span>

 Consider the statement PRINT# 1, A$, B$, C$. When used with the
 screen, the commas between the variables provide enough blank space
 between items to format them into columns ten characters wide. On
 cassette, anywhere from 1 to 10 spaces will be added, depending on th
 length of the strings.  This wastes space on your tape.

 Even worse is what happens when the INPUT# statement tries to read
 these strings. The statement INPUT# 1, A$, B$, C$ will discover no
 data for B$ and C$. A$ will contain all three variables, plus the
 spaces between them. What happens? Here's a look at the tape file:

``` basic
A$="DOG" B$="CAT" C$="TREE"
PRINT# 1, A$, B$, C$
```

``` c64
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
D O G                 C  A  T                       T  R  E  E  RETURN
```

 The INPUT# statement works like the regular INPUT statement. When
 typing data into the INPUT statement, the data items are separated,
 either by hitting the <kbd>RETURN</kbd> key or using commas to
 separate them. The PRINT# statement puts a RETURN at the end of a
 line just like the PRINT statement. A$ fills up with all three values
 because there's no separator on the tape between them, only after all
 three.

 A proper separator would be a comma (,) or a RETURN on the tape. The
 RETURN code is automatically put at the end of a PRINT or PRINT#
 statement. One way to put the RETURN code between each item is to us
 only one item per PRINT# statement. A better way is to set a variable
 to the RETURN CHR$ code, which is CHR$(13), or use a comma. The
 statement for this is R$=",":PRINT#1, A$ R$ B$ R$ C$. Don't use
 commas or any other punctuation between the variable names, since the
 Commodore 64 can tell them apart and they'll only use up space in
 your program.

 A proper tape file looks like this:

``` c64
1 2 3 4 5 6 7 8 9 10 11 12 13
D O G , C A T , T  R  E  E  RETURN
```

 The GET# statement will pick data from the tape one character at a
 time. It will receive each character, including the RETURN code and
 other punctuation. The CHR$(0) code is received as an empty string,
 not as a one character string with a code of 0. If you try to use the
 ASC function on an empty string, you get the error message ```ILLEGAL
 QUANTITY ERROR```.
 <span epub:type="pagebreak" id="p342" class="pagenumber">342</span>

 The line GET# 1, A$: A= ASC(A$) is commonly used in programs to
 examine tape data. To avoid error messages, the line should be
 modified to GET#1, A$: A=ASC(A$+CHR$(0)). The CHR$(0) at the end acts
 as insurance against empty strings, but doesn't affect the ASC
 function when there are other characters in A$.




### Data Storage on Floppy Diskettes

 Diskettes allow 3 different forms of data storage. Sequential files
 are similar to those on tape, but several can can be used at the same
 time.  Relative files let you organize the data into records, and
 then read and replace individual records within the file. Random
 files let you work with data anywhere on the disk. They are organized
 into 256 byte sections called blocks.

 The PRINT# statement's limitations are discussed in the section on
 cassette tape. The same limitations to format apply on the
 disk. RETURNs or commas are needed to separate your data. The CHR$(0)
 is still read by the GET# statement as an empty string.

 Relative and random files both make use of separate data and command
 “channels.” Data written to the disk goes through the data channel,
 where it is stored in a temporary buffer in the disk's RAM. When the
 record or block is complete, a command is sent through the command
 channel that tells the drive where to put the data, and the entire
 buffer is written.

 Applications that require large amounts of data to be processed are
 best stored in relative disk files. These will use the least amount
 of time and provide the best flexibility for the programmer. Your
 disk drive manual gives a complete programming guide to use of disk
 files.
 <span epub:type="pagebreak" id="p343" class="pagenumber">343</span>
 <a name="06-4">




## The Game Ports

 The Commodore 64 has two 9-pin Game Ports which allow the use of
 joysticks, paddles, or a light pen. Each port will accept either one
 joystick or one paddle pair. A light pen can be plugged into Port A
 (only) for special graphic control, etc. This section gives you
 examples of how to use the joysticks and paddles from both BASIC and
 machine language.

 The digital joystick is connected to CIA #1 (MOS 6526 Complex
 Interface Adapter). This input/output device also handles the paddle
 fire buttons and keyboard scanning. The 6526 CIA chip has 16
 registers which are in memory locations 56320 through 56335 inclusive
 ($DC00 to $DC0F). Port A data appears at location 56320 (DC00) and
 Port B data is found at location 56321 ($DC01).

 A digital joystick has five distinct switches, four of the switches
 are used for direction and one of the switches is used for the fire
 button.  The joystick switches are arranged as shown:

 ![Joystick Switches](images/06-344-1.png)

 These switches correspond to the lower 5 bits of the data in location
 56320 or 56321. Normally the bit is set to a one if a direction is
 NOT chosen or the fire button is NOT pressed. When the fire button is
 <span epub:type="pagebreak" id="p344" class="pagenumber">344</span>
 pressed, the bit (bit 4 in this case) changes to a 0. To read the
 joystick from BASIC, the following subroutine should be used:
 [06-344-1.bas](code/06-344-1.bas)


``` basic 06-344-1.bas
10 FOR K=0 TO 10 :REM SET UP DIRECTION STRING
20 READDR$(K) :NEXT
30 DATA"","N","S","","W","NW"
40 DATA"SW","","E","NE","SE"
50 PRINT"GOING...";
60 GOSUB 100 :REM READ THE JOYSTICK
65 IF DR$(JV)="" THEN 80 :REM CHECK IF A DIRECTION WAS CHOSEN
70 PRINT DR$(JV);" "; :REM OUTPUT WHICH DIRECTION
80 IF FR=16 THEN 60 :REM CHECK IF FIRE BUTTON WAS PUSHED
90 PRINT"-----F-----I-----R-----E-----!!!" :GOTO 60
100 JV=PEEK(56320) :REM GET JOYSTICK VALUE
110 FR=JV AND 16 :REM FORM FIRE BUTTON STATUS
120 JV=15-(JV AND 15) :REM FORM DIRECTION VALUE
130 RETURN
```

 > **NOTE:** For the second joystick, set JV = PEEK (56321).


 The values for JV correspond to these directions:

 | JV Equal To |   Direction   |
 | -----------:| -------------:|
 |      0      |          NONE |
 |      1      |            UP |
 |      2      |          DOWN |
 |      3      |             - |
 |      4      |          LEFT |
 |      5      |     UP & LEFT |
 |      6      |   DOWN & LEFT |
 |      7      |             - |
 |      8      |         RIGHT |
 |      9      |    UP & RIGHT |
 |     10      |  DOWN & RIGHT |

 <span epub:type="pagebreak" id="p345" class="pagenumber">345</span>


 A small machine code routine which accomplishes the same task is as
 follows:
 [06-345-1.asm](code/06-345-1.asm)


``` asm 06-345-1.asm
;;
;; joystick - button read routine
;;
;; author - bill hindorff
;;

dx = $c110
dy = $c111

* = $c200

djrr    lda $dc00     ; get input from port a only
djrrb   ldy #0        ; this routine reads and decodes the
        ldx #0        ; joystick/firebutton input data in
        lsr a         ; the accumulator. this least significant
        bcs djr0      ; 5 bits contain the switch closure
        dey           ; information. if a switch is closed then it
djr0    lsr a         ; produces a zero bit. if a switch is open then
        bcs djr1      ; it produces a one bit. The joystick dir-
        iny           ; ections are right, left, forward, backward
djr1    lsr a         ; bit3=right, bit2=left, bit1=backward,
        bcs djr2      ; bit0=forward and bit4=fire button.
        dex           ; at rts time dx and dy contain 2's compliment
djr2    lsr a         ; direction numbers i.e. $ff=-1, $00=0, $01=1.
        bcs djr3      ; dx=1 (move right), dx=-1 (move left),
        inx           ; dx=0 (no x change). dy=-1 (move up screen),
djr3    lsr a         ; dy=0 (move down screen), dy=0 (no y change).
        stx dx        ; the forward joystick position corresponds
        sty dy        ; to move up the screen and the backward
        rts           ; position to move down screen.
                      ;
                      ; at rts time the carry flag contains the fire
                      ; button state. if c=1 then button not pressed.
                      ; if c=0 then pressed.
.end
```

 <span epub:type="pagebreak" id="p346" class="pagenumber">346</span>




### Paddles

 A paddle is connected to both CIA #1 and the SID chip (MOS 6581 Sound
 Interface Device) through a game port. The paddle value is read via
 the SID registers 54297 ($D419) and 54298 ($D41A). PADDLES ARE NOT
 RELIABLE WHEN READ FROM BASIC ALONE!!!! The best way to use paddles,
 from BASIC or machine code, is to use the following machine language
 routine… (SYS to it from BASIC then PEEK the memory locations used by
 the subroutine).
 [06-346-1.asm](code/06-346-1.asm)


``` asm 06-346-1.asm
;;
;; four paddle read routine (can also be used for two)
;;
;; author - bill hindorff
;;

porta=$dc00
ciddra=$dc02
sid=$d400

*=$c100

buffer  *=*+1
pdlx    *=*+2
pdly    *=*+2
btna    *=*+1
btnb    *=*+1

* = $c000

pdlrd   ldx #1        ; for four paddles or two analog joysticks
pdlrd0                ; entry point for one pair (condition x 1st)
        sei
        lda ciddra    ; get current value of ddr
        sta buffer    ; save it away
        lda #$c0
        sta ciddra    ; set port a for input
        lda #$80
pdlrd1
        sta porta     ; address a pair of paddles
        ldy #$80      ; wait a while
pdlrd2
        nop
        dey
        bpl pdlrd2
        lda sid+25    ; get x value
        sta pdlx,x
        lda sid+26
        sta pdly,x    ; get y value
        lda porta     ; time to read paddle fire buttons
        ora #80       ; make it the same as other pair
        sta btna      ; bit 2 is pdl x, bit 3 is pdl y
        lda #$40
        dex           ; all pairs done?
        bpl pdlrd1    ; no
        lda buffer
        sta ciddra    ; restore previous value of ddr
        lda porta+1   ; for 2nd pair -
        sta btnb      ; bit 2 is pdl x, bit 3 is pdl y
        cli
        rts
.end
```

 <span epub:type="pagebreak" id="p347" class="pagenumber">347</span>

 The paddles can be read by using the following BASIC program:
 [06-347-1.bas](code/06-347-1.bas)


``` basic 06-347-1.bas
10 C=12*4096 :REM SET PADDLE ROUTINE START
11 REM POKE IN THE PADDLE READING ROUTINE
15 FOR I=0 TO 63 :READ A :POKE C+I,A :NEXT
20 SYS C :REM CALL THE PADDLE ROUTINE
30 P1=PEEK(C+257) :REM SET PADDLE ONE VALUE
40 P2=PEEK(C+258) :REM SET PADDLE TWO VALUE
50 P3=PEEK(C+259) :REM SET PADDLE THREE VALUE
60 P4=PEEK(C+260) :REM SET PADDLE FOUR VALUE
61 REM READ FIRE BUTTON STATUS
62 S1=PEEK(C+261) :S2=PEEK(C+262)
70 PRINT P1,P2,P3,P4 :REM PRINT PADDLE VALUES
72 REM PRINT FIRE BUTTON STATUS
75 PRINT :PRINT"FIRE A ";S1,"FIRE B ";S2
80 FOR W=1 TO 50 :NEXT :REM WAIT A WHILE
90 PRINT"{CLEAR}" :PRINT :GOTO 20 :REM CLEAR SCREEN AND DO AGAIN
95 REM DATA FOR MACHINE CODE ROUTINE
100 DATA 162,1,120,173,2,220,141,0,193,169,192,141,2,220,169
110 DATA 128,141,0,220,160,128,234,136,16,252,173,25,212,157
120 DATA 1,193,173,26,212,157,3,193,173,0,220,9,128,141,5,193
130 DATA 169,64,202,16,222,173,0,193,141,2,220,173,1,220,141
140 DATA 6,193,88,96
```
 <span epub:type="pagebreak" id="p348" class="pagenumber">348</span>




### LIGHT PEN

 The light pen input latches the current screen position into a pair
 of registers (LPX, LPY) on a low-going edge. The X position register
 19 ($13) will contain the 8 MSB of the X position at the time of
 transition.  Since the X position is defined by a 512-state counter
 (9 bits), resolution to 2 horizontal dots is provided. Similarly, the
 Y position is latched in its register 20 ($14), but here 8 bits
 provide single raster resolution within the visible display. The
 light pen latch may be triggered only once per frame, and subsequent
 triggers within the same frame will have no effect. Therefore, you
 must take several samples before turning the pen to the screen (3 or
 more samples average), depending upon the characteristics of your
 light pen.
 <a name="06-5">





## RS-232 Interface Description




### General Outline

 The Commodore 64 has a built-in RS-232 interface for connection to
 any RS-232 modem, printer, or other device. To connect a device to
 the Commodore 64, all you need is a cable and a little bit of
 programming.

 RS-232 on the Commodore 64 is set-up in the standard RS-232 format,
 but the voltages are TTL levels (0 to 5V) rather than the normal
 RS-232 -12 to 12 volt range. The cable between the Commodore 64 and
 the RS-232 device should take care of the necessary voltage
 conversions. The Commodore RS-232 interface cartridge handles this
 properly.

 The RS-232 interface software can be accessed from BASIC or from the
 KERNAL for machine language programming.

 RS-232 on the BASIC level uses the normal BASIC commands: OPEN,
 CLOSE, CMD, INPUT#, GET#, PRINT#, and the reserved variable
 ST. INPUT# and GET# fetch data from the receiving buffer, while
 PRINT# and CMD place data into the transmitting buffer. The use of
 these commands (and examples) will be described in more detail later
 in this chapter.

 The RS-232 KERNAL byte and bit level handlers run under the control
 of the 6526 CIA #2 device timers and interrupts. The 6526 chip
 generates
 <span epub:type="pagebreak" id="p349" class="pagenumber">349</span>
 NMI (Non-Maskable Interrupt) requests for RS-232 processing. This
 allows background RS-232 processing to take place during BASIC and
 machine language programs. There are built-in hold-offs in the
 KERNAL, cassette, and serial bus routines to prevent the disruption
 of data storage or transmission by the NMIs that are generated by the
 RS-232 routines.  During cassette or serial bus activities, data can
 NOT be received from RS-232 devices. But because these hold-offs are
 only local (assuming you're careful about your programming) no
 interference should result.

 There are two buffers in the Commodore 64 RS-232 interface to help
 prevent the loss of data when transmitting or receiving RS-232
 information.

 The Commodore 64 RS-232 KERNAL buffers consist of two
 first-in/first-out (FIFO) buffers, each 256 bytes long, at the top of
 memory. The OPENing of an RS-232 channel automatically allocates 512
 bytes of memory for these buffers. If there is not enough free space
 beyond the end of your BASIC program no error message will be
 printed, and the end of your program will be destroyed. SO BE
 CAREFUL!

 These buffers are automatically removed by using the CLOSE command.




### Opening an RS-232 Channel

 Only one RS-232 channel should be open at any time; a second OPEN
 statement will cause the buffer pointers to be reset. Any characters
 in either the transmit buffer or the receive buffer will be lost.

 Up to 4 characters can be sent in the filename field. The first two
 are the control and command register characters; the other two are
 reserved for future system options. Baud rate, parity, and other
 options can be selected through this feature.

 No error-checking is done on the control word to detect a
 nonimplemented baud rate. Any illegal control word will cause the
 system output to operate at a very slow rate (below 50 baud).



#### BASIC Syntax:

 OPEN lfn,2,0,"\<control register\>\<command register\>\<opt baud low\>\<opt
 baud high\>"

 lfn-The logical file number (lfn) then can be any number from 1
 through 255. But be aware of the fact that if you choose a logical
 file number that is greater than 127, then a line feed will follow
 all carriage returns.
 <span epub:type="pagebreak" id="p350" class="pagenumber">350</span>

 ![Control Register Map](images/06-350-1.png)

```
                   +-+-+-+ +-+ +-+-+-+-+
                   |7|6|5| |4| |3|2|1|0|
                   +-+-+-+ +-+ +-+-+-+-+   BAUD RATE
                    | | |   |  +-+-+-+-+----------------+
      STOP BITS ----+ | |   |  |0|0|0|0| USER RATE  [NI]|
                      | |   |  +-+-+-+-+----------------+
   0 - 1 STOP BIT     | |   |  |0|0|0|1|       50 BAUD  |
   1 - 2 STOP BITS    | |   |  +-+-+-+-+----------------+
                      | |   |  |0|0|1|0|       75       |
                      | |   |  +-+-+-+-+----------------+
                      | |   |  |0|0|1|1|      110       |
                      | |   |  +-+-+-+-+----------------+
     WORD LENGTH -----+-+   |  |0|1|0|0|      134.5     |
                            |  +-+-+-+-+----------------+
  +---+-----------+         |  |0|1|0|1|      150       |
  |BIT|           |         |  +-+-+-+-+----------------+
  +-+-+    DATA   |         |  |0|1|1|0|      300       |
  |6|5|WORD LENGTH|         |  +-+-+-+-+----------------+
  +-+-+-----------+         |  |0|1|1|1|      600       |
  |0|0|  8 BITS   |         |  +-+-+-+-+----------------+
  +-+-+-----------+         |  |1|0|0|0|     1200       |
  |0|1|  7 BITS   |         |  +-+-+-+-+----------------+
  +-+-+-----------+         |  |1|0|0|1|    (1800)  2400|
  |1|0|  6 BITS   |         |  +-+-+-+-+----------------+
  +-+-+-----------+         |  |1|0|1|0|     2400       |
  |1|1|  5 BITS   |         |  +-+-+-+-+----------------+
  +-+-+-----------+         |  |1|0|1|1|     3600   [NI]|
                            |  +-+-+-+-+----------------+
                            |  |1|1|0|0|     4800   [NI]|
        UNUSED -------------+  +-+-+-+-+----------------+
                               |1|1|0|1|     7200   [NI]|
                               +-+-+-+-+----------------+
                               |1|1|1|0|     9600   [NI]|
                               +-+-+-+-+----------------+
                               |1|1|1|1|    19200   [NI]|
                               +-+-+-+-+----------------+
```
 _Figure 6-1. Control Register Map._

 \<control register\> — Is a single byte character (see Figure 6-1,
 Control Register Map) required to specify the baud rates. If the
 lower 4 bits of the baud rate is equal to zero (0), the \<opt baud
 low\>\<opt baud high\> characters give you a rate based on the
 following:\
 \<opt baud low\>=\<system frequency/rate/2-100-\<opt baud high\>*256\
 \<opt baud high\>=INT((system frequency/rate/2-100)/256

 <span epub:type="pagebreak" id="p351" class="pagenumber">351</span>

 ![Command Register Map](images/06-351-1.png)

```
                              +-+-+-+-+-+-+-+-+
                              |7|6|5|4|3|2|1|0|
                              +-+-+-+-+-+-+-+-+
                               | | | | | | | |
                               | | | | | | | |
                               | | | | | | | |
                               | | | | | | | |
            PARITY OPTIONS ----+-+-+ | | | | +----- HANDSHAKE
  +---+---+---+---------------------+| | | |
  |BIT|BIT|BIT|     OPERATIONS      || | | |        0 - 3-LINE
  | 7 | 6 | 5 |                     || | | |        1 - X-LINE
  +---+---+---+---------------------+| | | |
  | - | - | 0 |PARITY DISABLED, NONE|| | | |
  |   |   |   |GENERATED/RECEIVED   || | | |
  +---+---+---+---------------------+| | | +------- UNUSED
  | 0 | 0 | 1 |ODD PARITY           || | +--------- UNUSED
  |   |   |   |RECEIVER/TRANSMITTER || +----------- UNUSED
  +---+---+---+---------------------+|
  | 0 | 1 | 1 |EVEN PARITY          ||
  |   |   |   |RECEIVER/TRANSMITTER |+------------- DUPLEX
  +---+---+---+---------------------+
  | 1 | 0 | 1 |MARK TRANSMITTED     |               0 - FULL DUPLEX
  |   |   |   |PARITY CHECK DISABLED|               1 - HALF DUPLEX
  +---+---+---+---------------------+
  | 1 | 1 | 1 |SPACE TRANSMITTED    |
  |   |   |   |PARITY CHECK DISABLED|
  +---+---+---+---------------------+
```

 _Figure 6-2. Command Register Map._

 The formulas above are based on the fact that:

 system frequency\
 = 1.02273E6 NTSC (North American TV standard)\
 = 0.98525E6 PAL (U.K. and most European TV standard)

 \<command register\> — Is a single byte character (see Figure 6-2,
 Command Register Map) that defines other terminal parameters. This
 character is NOT required.
 <span epub:type="pagebreak" id="p352" class="pagenumber">352</span>




#### Kernal Entry:

 OPEN ($FFC0) (See KERNAL specifications for more information on entry
 conditions and instructions.)


 > **IMPORTANT NOTE:** In a BASIC program, the RS-232 OPEN command
   should be performed before creating any variables or arrays
   because an automatic CLR is performed when an RS-232 channel is
   OPENed (This is due to the allocation of 512 bytes at the top of
   memory.) Also remember that your program will be destroyed if 512
   bytes of space are not available at the time of the OPEN
   statement.




### Getting Data From an RS-232 Channel

 When getting data from an RS-232 channel, the Commodore 64 receiver
 buffer will hold up to 255 characters before the buffer
 overflows. This is indicated in the RS-232 status word (ST in BASIC,
 or RSSTAT in machine language). If an overflow occurs, then all
 characters received during a full buffer condition, from that point
 on, are lost. Obviously, it pays to keep the buffer as clear as
 possible.

 If you wish to receive RS-232 data at high speeds (BASIC can only go
 so fast, especially considering garbage collects. This can cause the
 receiver buffer to overflow), you will have to use machine language
 routines to handle this type of data burst.




#### BASIC Syntax:

 Recommended: GET#lfn, \<string variable\>
 NOT Recommended: INPUT#lfn \<variable list\>



#### KERNAL Entries:

 **CHKIN** ($FFC6) — See Memory Map for more information on entry and
   exit conditions.

 **GETIN** ($FFE4) — See Memory Map for more information on entry and
   exit conditions.

 **CHRIN** ($FFCF) — See Memory Map for more information on entry and
   exit conditions.

 <span epub:type="pagebreak" id="p353" class="pagenumber">353</span>


 > **NOTES:** If the word length is less than 8 bits, all unused
   bit(s) will be assigned a value of zero.

 > If a GET# does not find any data in the buffer, the character ""
   (a null) is returned.

 > If INPUT# is used, then the system will hang in a waiting condition
   until a non-null character and a following carriage return is
   received. Therefore, if the Clear To Send (CTS) or Data Set Ready
   (DSR) line(s) disappear during character INPUT#, the system will
   hang in a RESTORE-only state. This is why the INPUT# and CHRIN
   routines are NOT recommended.

 > The routine CHKIN handles the x-line handshake which follows the
   EIA standard (August 1979) for RS-232-C interfaces. (The Request To
   Send (RTS), CTS, and Received line signal (DCD) lines are
   implemented with the Commodore 64 computer defined as the Data
   Terminal device.)




### Sending Data to an RS-232 Channel

 When sending data, the output buffer can hold 255 characters before a
 full buffer hold-off occurs. The system will wait in the CHROUT
 routine until transmission is allowed or the <kbd>RUN/STOP</kbd> and
 <kbd>RESTORE</kbd> keys are used to recover the system through a WARM
 START.


#### BASIC Syntax:

 CMD lfn-acts same as in the BASIC specifications.
 PRINT#lfn,\<variable list\>


#### KERNAL Entries:

 **CHKOUT** ($FFC9) — See Memory Map for more information on entry and
   exit conditions.

 **CHROUT** ($FFD2) — See Memory Map for more information on entry
   conditions.

 <span epub:type="pagebreak" id="p354" class="pagenumber">354</span>


 > **IMPORTANT NOTES:** There is no carriage-return delay built into
   the output channel. This means that a normal RS-232 printer
   cannot correctly print, unless some form of hold-off (asking the
   Commodore 64 to wait) or internal buffering is implemented by the
   printer. The hold-off can easily be implemented in your
   program. If a CTS (x-line) handshake is implemented, the
   Commodore 64 buffer will fill, and then hold-off more output
   until transmission is allowed by the RS-232 device. X-line
   handshaking is a handshake routine that uses multi- lines for
   receiving and transmitting data.

 > The routine CHKOUT handles the x-line handshake, which follows the
   EIA standard (August 1979) for RS-232-C interfaces. The RTS, CTS,
   and DCD lines are implemented with the Commodore 64 defined as the
   Data Terminal Device.




### Closing an RS-232 Data Channel

 Closing an RS-232 file discards all data in the buffers at the time
 of execution (whether or not it had been transmitted or printed out),
 stops all RS-232 transmitting and receiving, sets the RTS and
 transmitted data (Sout) lines high, and removes both RS-232 buffers.


#### Basic Syntax:

 **CLOSE** lfn


#### KERNAL Entry:

 **CLOSE** ($FFC3) — See Memory Map for more information on entry and
   exit conditions.

 > **NOTE:** Care should be taken to ensure all data is transmitted
     before closing the channel. A way to check this from BASIC is:

``` basic
100 SS=ST: IF(SS=0 OR SS=8) THEN 100
110 CLOSE lfn
```
 <span epub:type="pagebreak" id="p355" class="pagenumber">355</span>


 _Table 6-1. User-Port Lines_

 (6526 DEVICE #2 Loc. $DD00-$DD0F)

 |PIN ID|6526 ID|      DESCRIPTION     | EIA  |  ABV  | IN/OUT | MODES |
 |:----:|:-----:|:-------------------- |:----:|:----- |:------ | ----- |
 |  C   |  PB0  | RECEIVED DATA        | (BB) |  Sin  |   IN   | 1 2   |
 |  D   |  PB1  | REQUEST TO SEND      | (CA) |  RTS  |   OUT  | 1*2   |
 |  E   |  PB2  | DATA TERMINAL READY  | (CD) |  DTR  |   OUT  | 1*2   |
 |  F   |  PB3  | RING INDICATOR       | (CE) |  RI   |   IN   |     3 |
 |  H   |  PB4  | RECEIVED LINE SIGNAL | (CF) |  DCD  |   IN   |   2   |
 |  I   |  PB5  | UNASSIGNED           | (  ) |  XXX  |   IN   |     3 |
 |  K   |  PB6  | CLEAR TO SEND        | (CB) |  CTS  |   IN   |   2   |
 |  L   |  PB7  | DATA SET READY       | (CC) |  DSR  |   IN   |   2   |
 |  B   | FLAG2 | RECEIVED DATA        | (BB) |  Sin  |   IN   | 1 2   |
 |  M   |  PA2  | TRANSMITTED DATA     | (BA) |  Sout |   OUT  | 1 2   |
 |  A   |  GND  | PROTECTIVE GROUND    | (AA) |  GND  |        | 1 2   |
 |  N   |  GND  | SIGNAL GROUND        | (AB) |  GND  |        | 1 2 3 |

 MODES:

 1. 3-LINE INTERFACE (Sin,Sout,GND)
 2. X-LINE INTERFACE
 3. USER AVAILABLE ONLY (Unused/unimplemented in code.)


 \* These lines are held high during 3-LINE mode.

 ![RS232 Status Register](images/06-355-1.png)

```
[7] [6] [5] [4] [3] [2] [1] [0] (Machine Lang.-RSSTAT
 |   |   |   |   |   |   |   +- PARITY ERROR BIT
 |   |   |   |   |   |   +----- FRAMING ERROR BIT
 |   |   |   |   |   +--------- RECEIVER BUFFER OVERRUN BIT
 |   |   |   |   +------------- RECEIVER BUFFER-EMPTY
 |   |   |   |                  (USE TO TEST AFTER A GET#)
 |   |   |   +----------------- CTS SIGNAL MISSING BIT
 |   |   +--------------------- UNUSED BIT
 |   +------------------------- DSR SIGNAL MISSING BIT
 +----------------------------- BREAK DETECTED BIT

```

 _Figure 6-3. RS-232 Status Register._

 <span epub:type="pagebreak" id="p356" class="pagenumber">356</span>


 > **NOTES:** If the BIT=0, then no error has been detected. The
   RS-232 status register can be read from BASIC using the variable ST.

 > If ST is read by BASIC or by using the KERNAL READST routine the
   RS-232 status word is cleared when you exit. If multiple uses of
   the STATUS word are necessary the ST should be assigned to another
   variable. For example: ```SR=ST: REM ASSIGNS ST TO SR```

 > The RS-232 status is read (and cleared) only when the RS-232
   channel was the last external I/O used.


#### Sample BASIC Programs

 [06-356-1.bas](code/06-356-1.bas)

``` basic 06-356-1.bas
10 REM THIS PROGRAM SENDS AND RECEIVES DATA TO/FROM A SILENT 700
11 REM TERMINAL MODIFIED FOR PET ASCII
20 REM TI SILENT 700 SET-UP: 300 BAUD, 7-BIT ASCII, MARK PARITY,
21 REM FULL DUPLEX
30 REM SAME SET-UP AT COMPUTER USING 3-LINE INTERFACE
100 OPEN2,2,3,CHR$(6+32)+CHR$(32+128):REM OPEN THE CHANNEL
110 GET#2,A$:REM TURN ON THE RECEIVER CHANNEL (TOSS A NULL)
200 REM MAIN LOOP
210 GET B$:REM GET FROM COMPUTER KEYBOARD
220 IF B$<>""THEN PRINT#2,B$;:REM IF A KEY PRESSED, SEND TO TERMINAL
230 GET#2,C$:REM GET A KEY FROM THE TERMINAL
240 PRINT B$;C$;:REM PRINT ALL INPUTS TO COMPUTER SCREEN
250 SR=ST:IFSR=0ORSR=8THEN200:REM CHECK STATUS, IF GOOD THEN CONTINUE
300 REM ERROR REPORTING
310 PRINT "ERROR: ";
320 IF SR AND 1 THEN PRINT"PARITY"
330 IF SR AND 2 THEN PRINT"FRAME"
340 IF SR AND 4 THEN PRINT"RECEIVER BUFFER FULL"
350 IF SR AND 128 THEN PRINT"BREAK"
360 IF (PEEK(673)AND1)THEN360:REM WAIT UNTIL ALL CHARS TRANSMITTED
370 CLOSE 2:END
```

 <span epub:type="pagebreak" id="p357" class="pagenumber">357</span>
 [06-357-1.bas](code/06-357-1.bas)
 

``` basic 06-357-1.bas
10 REM THIS PROGRAM SENDS AND RECEIVES TRUE ASCII DATA
100 OPEN 5,2,3,CHR$(6)
110 DIM F%(255),T%(255)
200 FOR J=32 TO 64:T%(J)=J:NEXT
210 T%(13)=13:T%(20)=8:RV=18:CT=0
220 FOR J=65 TO 90:K=J+32:T%=(J)=K:NEXT
230 FOR J=91 TO 95:T%(J)=J:NEXT
240 FOR J=193 TO 218:K=J-128:T%(J)=K:NEXT
250 T%(146)=16:T%(133)=16
260 FOR J=0 TO 255
270 K=T%(J)
280 IF K<>0THEN F%(K)=J:F%(K+128)=J
290 NEXT
300 PRINT" "CHR$(147)
310 GET#5,A$
320 IF A$=""OR ST<>0 THEN 360
330 PRINT" "CHR$(157);CHR$(F%(ASC(A$)));
340 IF F%(ASC(A$))=34 THEN POKE212,0
350 GOTO310
360 PRINTCHR$(RV)" "CHR$(157);CHR$(146);:GET A$
370 IF A$<>""THEN PRINT#5,CHR$(T%(ASC(A$)));
380 CT=CT+1
390 IF CT=8 THENCT=0:RV=164-RV
410 GOTO310
```




### Receiver/Transmitter Buffer Base Location Pointers

 **$00F7-REBUF** — A two-byte pointer to the Receiver Buffer base location.

 **$00F9-ROBUF** — A two-byte pointer to the Transmitter Buffer base location.

 The two locations above are set up by the OPEN KERNAL routine, each
 pointing to a different 256-byte buffer. They are de-allocated by
 writing a zero into the high order bytes ($00F8 and $00FA), which is
 done by the CLOSE KERNAL entry. They may also be
 allocated/de-allocated by the machine language programmer for his/her
 own purposes, removing/creating only the buffer(s) required. When
 using a machine language program that allocates these buffers, care
 must be taken to make sure that the top of memory pointers stay
 correct, especially if BASIC programs are expected to run at the same
 time.
 <span epub:type="pagebreak" id="p358" class="pagenumber">358</span>




### Zero-Page Memory Locations and Usage for RS-232 System Interface

 **$00A7-INBIT** — Receiver input bit temp storage.

 **$00A8-BITCI** — Receiver bit count in.

 **$00A9-RINONE** — Receiver flag Start bit check.

 **$00AA-RIDATA** — Receiver byte buffer/assembly location.

 **$00AB-RIPRTY** — Receiver parity bit storage.

 **$00B4-BITTS** — Transmitter bit count out.

 **$00B5-NXTBIT** — Transmitter next bit to be sent.

 **$00B6-RODATA** — Transmitter byte buffer/disassembly location.


 All the above zero-page locations are used locally and are only given
 as a guide to understand the associated routines. These cannot be
 used directly by the BASIC or KERNAL level programmer to do RS-232
 type things. The system RS-232 routines must be used.




### Nonzero-Page Memory Locations and Usage for RS-232 System Interface

 General RS-232 storage:

 **$0293-M51CTR** — Pseudo 6551 control register (see Figure 6-1).

 **$0294-M51COR** — Pseudo 6551 command register (see Figure 6-2).

 **$0295-M51AJB** — Two bytes following the control and command registers in the file name field. These locations contain the baud rate for the start of the bit test during the interface activity, which, in turn, is used to calculate baud rate.

 **$0297-RSSTAT** — The RS-232 status register (see Figure 6-3).

 **$0298-BITNUM** — The number of bits to be sent/received.

 **$0299-BAUDOF** — Two bytes that are equal to the time of one bit cell. (Based on system clock/baud rate.)
 <span epub:type="pagebreak" id="p359" class="pagenumber">359</span>


 **$029B-RIDBE** — The byte index to the end of the receiver FIFO buffer.

 **$029C-RIDBS** — The byte index to the start of the receiver FIFO buffer.

 **$029D-RODBS** — The byte index to the start of the transmitter FIFO buffer.

 **$029E-RODBE** — The byte index to the end of the transmitter FIFO buffer.

 **$02A1-ENABL** — Holds current active interrupts in the CIA #2 ICR. When bit 4 is turned on means that the system is waiting for the Receiver Edge. When bit 1 is turned on then the system is receiving data. When bit 0 is turned on then the system is transmitting data.
 <a name="06-6">




## The User Port

 The user port is meant to connect the Commodore 64 to the outside
 world. By using the lines available at this port, you can connect the
 Commodore 64 to a printer, a Votrax Type and Talk, a MODEM, even
 another computer.

 The port on the Commodore 64 is directly connected to one of the 6526
 CIA chips. By programming, the CIA will connect to many other
 devices.




### Port Pin Description

 ![Port Pin Description](images/06-360-1.png)

```
                      1 1 1
    1 2 3 4 5 6 7 8 9 0 1 2
 +--@-@-@-@-@-@-@-@-@-@-@-@--+
 |                           |
 +--@-@-@-@-@-@-@-@-@-@-@-@--+
    A B C D E F H J K L M N
```

 <span epub:type="pagebreak" id="p360" class="pagenumber">360</span>


 _Port Pin Description_

 | TOP PIN   |DESCRIPTION|          NOTES                                |
 |:---------:|:---------:|:--------------------------------------------- |
 |     1     |  GROUND   |                                               |
 |     2     |   +5V     |  (100 mA MAX.)                                |
 |     3     |  RESET    |  By grounding this pin, the Commodore 64 will do a COLD START, resetting completely. The pointers to a BASIC program will be reset, but memory will not be cleared. This is also a RESET output for the external devices. |
 |     4     |    CNT1   |  Serial port counter from CIA#1(SEE CIA SPECS)|
 |     5     |    SP1    |  Serial port from CIA #l (SEE 6526 CIA SPECS) |
 |     6     |    CNT2   |  Serial port counter from CIA#2(SEE CIA SPECS)|
 |     7     |    SP2    |  Serial port from CIA #l (SEE 6526 CIA SPECS) |
 |     8     |    PC2    |  Handshaking line from CIA #2 (SEE CIA SPECS) |
 |     9     |SERIAL ATN |  This pin is connected to the ATN line of the serial bus. |
 |    10     |9 VAC+phase|  Connected directly to the Commodore          |
 |    11     |9 VAC-phase|  64 transformer (50 mA MAX.).                 |
 |    12     |    GND    |                                               |

 |BOTTOM PIN |DESCRIPTION|          NOTES                                |
 |:---------:|:---------:|:--------------------------------------------- |
 |     A     |    GND    |  The Commodore 64 gives you control over      |
 |     B     |   FLAG2   |  PORT B on CIA chip #1. Eight lines for input |
 |     C     |    PB0    |  or output are available, as well as 2 lines  |
 |     D     |    PB1    |  for handshaking with an outside device. The  |
 |     E     |    PB2    |  I/O lines for PORT B are controlled by two   |
 |     F     |    PB3    |  locations. One is the PORT itself, and is    |
 |     H     |    PB4    |  located at 56577 ($DD01 HEX). Naturally you  |
 |     I     |    PB5    |  PEEK it to read an INPUT, or POKE it to set  |
 |     K     |    PB6    |  an OUTPUT. Each of the eight I/O lines can   |
 |     L     |    PB7    |  be set up as either an INPUT or an OUTPUT by |
 |     M     |    PA2    |  by setting the DATA DIRECTION REGISTER       |
 |     N     |    GND    |  properly.                                    |

 <span epub:type="pagebreak" id="p361" class="pagenumber">361</span>


 The DATA DIRECTION REGISTER has its location at 56579 ($DD03 hex).
 Each of the eight lines in the PORT has a BIT in the eight-bit
 DATA DIRECTION REGISTER (DDR) which controls whether that line will
 be an input or an output. If a bit in the DDR is a ONE, the
 corresponding line of the PORT will be an OUTPUT. If a bit in the DDR
 is a ZERO, the corresponding line of the PORT will be an INPUT. For
 example, if bit 3 of the DDR is set to 1, then line 3 of the PORT
 will be an output. A further example:

 If the DDR is set like this:

```
BIT #: 7 6 5 4 3 2 1 0
VALUE: 0 0 1 1 1 0 0 0
```

 You can see that lines 5,4, and 3 will be outputs since those bits
 are ones. The rest of the lines will be inputs, since those lines are
 zeros.

 To PEEK or POKE the USER port, it is necessary to use both the DDR
 and the PORT itself.

 Remember that the PEEK and POKE statements want a number from 0-255.
 The numbers given in the example must be translated into decimal
 before they can be used. The value would be:

```
2^5 + 2^4 + 2^3 = 32 + 16 + 8 = 56
```

 Notice that the bit # for the DDR is the same number that = 2 raised
 to a power to turn the bit value on.

```
(16 = 2^4=2*2*2*2, 8 = 2^3=2*2*2)
```

 The two other lines, FLAG1 and PA2 are different from the rest of the
 USER PORT. These two lines are mainly for HANDSHAKING, and are
 programmed differently from port B.

 Handshaking is needed when two devices communicate. Since one device
 may run at a different speed than another device it is necessary to
 give the devices some way of knowing what the other device is
 doing. Even when the devices are operating at the same speed,
 handshaking is necessary to let the other know when data is to be
 sent, and if it has been received.  The FLAG1 line has special
 characteristics which make it well suited for handshaking.

 FLAG1 is a negative edge sensitive input which can be used as a
 general purpose interrupt input. Any negative transition on the FLAG
 line will set the FLAG interrupt bit. If the FLAG interrupt is
 enabled, this will
 <span epub:type="pagebreak" id="p362" class="pagenumber">362</span>
 cause an INTERRUPT REQUEST. If the FLAG bit is not enabled, it can be
 polled from the interrupt register under program control.

 PA2 is bit 2 of PORT A of the CIA. It is controlled like any other
 bit in the port. The port is located at 56576 ($DD00). The data
 direction register is located at 56578 ($DD02.)

 For more information on the 6526 see the chip specifications in
 APPENDIX M.
 <a name="06-7">





## The Serial Bus

 The serial bus is a daisy chain arrangement designed to let the
 Commodore 64 communicate with devices such as the VIC-1541 DISK DRIVE
 and the VIC-1525 GRAPHICS PRINTER. The advantage of the serial bus is
 that more than one device can be connected to the port. Up to 5
 devices can be connected to the serial bus at one time.

 There are three types of operation over a serial bus — CONTROL, TALK,
 and LISTEN. A CONTROLLER device is one which controls operation of
 the serial bus. A TALKER transmits data onto the bus. A LISTENER
 receives data from the bus.

 The Commodore 64 is the controller of the bus. It also acts as a
 TALKER (when sending data to the printer, for example) and as a
 LISTENER (when loading a program from the disk drive, for
 example). Other devices may be either LISTENERS (the printer),
 TALKERS, or both (the disk drive). Only the Commodore 64 can act as
 the controller.

 All devices connected on the serial bus will receive all the data
 transmitted over the bus. To allow the Commodore 64 to route data to
 its intended destination, each device has a bus ADDRESS. By using
 this device address, the Commodore 64 can control access to the
 bus. Addresses on the serial bus range from 4 to 31.

 The Commodore 64 can COMMAND a particular device to TALK or LISTEN.
 When the Commodore 64 commands a device to TALK, the device will
 begin putting data onto the serial bus. When the Commodore 64
 commands a device to LISTEN, the device addressed will get ready to
 receive data (from the Commodore 64 or from another device on the
 bus). Only one device can TALK on the bus at a time; otherwise, the
 data will collide and the system will crash in confusion. However,
 any number of devices can LISTEN at the same time to one TALKER.
 <span epub:type="pagebreak" id="p363" class="pagenumber">363</span>


 _Common Serial Bus Addresses_

 | NUMBER |        DEVICE            |
 |:------ |:------------------------ |
 | 4 or 5 | VIC-1525 GRAPHIC PRINTER |
 | 8      | VIC-1541 DISK DRIVE      |


 Other device addresses are possible. Each device has its own address.
 Certain devices (like the Commodore 64 printer) provide a choice
 between two addresses for the convenience of the user.

 The SECONDARY ADDRESS is to let the Commodore 64 transmit setup
 information to a device. For example, to OPEN a connection on the bus
 to the printer, and have it print in UPPER/LOWER case, use the
 following

``` basic
OPEN 1,4,7
```

 where,\
 1 is the logical file number (the number you PRINT# to),\
 4 is the ADDRESS of the printer, and\
 7 is the SECONDARY ADDRESS that tells the printer to go into UPPER/
   LOWER case mode.

 There are 6 lines used in serial bus operations — input and 3 output.
 The 3 input lines bring data, control, and timing signals into the
 Commodore 64. The 3 output lines send data, control, and timing
 signals from the Commodore 64 to external devices on the serial bus.

 Serial I/O

 |  Pin  |         Type         |
 |:-----:|:-------------------- |
 |   1   |  Serial SRQ In       |
 |   2   |  GND                 |
 |   3   |  Serial ATN Out      |
 |   4   |  Serial CLK In/Out   |
 |   5   |  Serial DATA In/Out  |
 |   6   |  No Connection/Reset |


 ![Serial Bus Pinouts](images/06-364-1.png)

```
     ---+ +---
    /   +-+   \
   / 5       1 \
  +   O     O   +
  |      6      |
  |      O      |
  |             |
  +   O     O   +
   \ 4   O   2 /
    \    3    /
     +-------+
```

 <span epub:type="pagebreak" id="p364" class="pagenumber">364</span>



#### Serial SRQ IN: (Serial Service Request In)

 Any device on the serial bus can bring this signal LOW when it
 requires attention from the Commodore 64. The Commodore 64 will then
 take care of the device. (See Figure 6-4).


 ![Figure 6-4. Serial Bus Timing](images/06-364-1.png)

 _Figure 6-4. Serial Bus Timing._



 <span epub:type="pagebreak" id="p365" class="pagenumber">365</span>



#### Serial ATN Out: (Serial Attention Out)

 The Commodore 64 uses this signal to start a command sequence for a
 device on the serial bus. When the Commodore 64 brings this signal
 LOW, all other devices on the bus start listening for the Commodore
 64 to transmit an address. The device addressed must respond in a
 preset period of time; otherwise, the Commodore 64 will assume that
 the device addressed is not on the bus, and will return an error in
 the STATUS WORD.  (See Figure 6-4).


 ![Serial Bus Timing](images/06-365-1.png)

```
[THE PICTURE IS MISSING!]
```

 _Serial Bus Timing_
															
 |     Description             | Symbol|  Min. |  Typ. |    Max.  |
 |:--------------------------- |:----- |:-----:|:-----:|:--------:|
 | ATN Response (Required) (1) |  Tₐₜ  |   -   |   -   |  1000us  |
 | Listener Hold-Off           |  Tₕ   |   0   |   -   | infinite |
 | Non-EOI Response to RFD (2) |  Tₙₑ  |   -   |  40us |   200us  |
 | Bit Set-Up Talker (4)       |  Tₛ   |  20us |  70us |     -    |
 | Data Valid                  |  Tv   |  20us |  20us |     -    |
 | Frame Handshake (3)         |  Tf   |   0   |  20   |  1000us  |
 | Frame to Release of ATN     |  Tr   |  20us |   -   |     -    |
 | Between Bytes Time          |  Tbb  | 100us |   -   |     -    |
 | EOI Response Time           |  Tye  | 200us | 250us |     -    |
 | EOI Response Hold Time (5)  |  Tei  |  60us |   -   |     -    |
 | Talker Response Limit       |  Try  |   0   |  30us |    60us  |
 | Byte-Acknowledge (4)        |  Tpr  |  20us |  30us |     -    |
 | Talk-Attention Release      |  Ttk  |  20us |  30us |   100us  |
 | Talk-Attention Acknowledge  |  Tdc  |   0   |   -   |     -    |
 | Talk-Attention Ack. Hold    |  Tda  |  80us |   -   |     -    |
 | EOI Acknowledge             |  Tfr  |  60us |   -   |     -    |


Notes:

 1. If maximum time exceeded, device not present error.
 2. If maximum time exceeded, EOI response required.
 3. If maximum time exceeded, frame error.
 4. Tv and Tpr minimum must be 60us for external device to be a talker.
 5. Tei minimum must be 80us for external device to be a listener.


 <span epub:type="pagebreak" id="p366" class="pagenumber">366</span>




#### Serial CLK In/Out: (Serial Clock In/Out)

 This signal is used for timing the data sent on the serial bus.
 (See Figure 6-4).




#### Serial Data In/Out:

 Data on the serial bus is transmitted one bit at a time on this line.
 (See Figure 6-4.)
 <a name="06-8">




## The Expansion Port

 The expansion connector is a 44-pin (22122) female edge connector on
 the back of the Commodore 64. With the Commodore 64 facing you, the
 expansion connector is on the far right of the back of the
 computer. To use the connector, a 44-pin (22/22) male edge connector
 is required.

 This port is used for expansions of the Commodore 64 system which
 require access to the address bus or the data bus of the computer.
 Caution is necessary when using the expansion bus, because it's
 possible to damage the Commodore 64 by a malfunction of your
 equipment.  The expansion bus is arranged as follows:


 ![Expansion Port Pinouts](images/06-366-1.png)

```
     2 2 2 1 1 1 1 1 1 1 1 1 1
     2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1
 +---@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@---+
 |                                                 |
 +---@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@-@---+
     Z Y X W V U T S R P N M L K J H F E D C B A
```

 The signals available on the connector are as follows:

 |   Name  |Pin|                       Description                       |
 |:-------:|:-:|:------------------------------------------------------- |
 |   GND   | 1 |  System ground                                          |
 |  +5VDC  | 2 |  (Total USER PORT and CARTRIDGE devices can             |
 |  +5VDC  | 3 |  draw no more than 450 mA.)                             |
 |  IRQ*   | 4 |  Interrupt Request line to 6502 (active low)            |
 |   R/W   | 5 |  Read/Write (write active low)                          |
 |DOT CLOCK| 6 |  8.18 MHz video dot clock                               |
 |  I/O1*  | 7 |  I/O block 1 @ $ DE00-$DEFF (active low) unbuffered I/O |
 |  GAME*  | 8 |  active low ls ttl input                                |
 |  EXROM* | 9 |  active low ls ttl input                                |
 |  I/O2*  |10 |  I/O block 2 @ $DF00-$DFFF (active low) buff'ed ls ttl output |

 \* Active low
 <span epub:type="pagebreak" id="p367" class="pagenumber">367</span>


 |   Name  |Pin|                       Description                       |
 |:-------:|:-:|:------------------------------------------------------- |
 |  ROML*  |11 |  8K decoded RAM/ROM block @ $8000 (active low) buffered ls ttl output |
 |   BA    |12 |  Bus available signal from the VIC-II chip unbuffered 1 Is load max. |
 |  DMA*   |13 |  Direct memory access request line (active low input) ls ttl input |
 |   D7    |14 |  Data bus bit 7      unbuffered, 1 ls ttl load max      |
 |   D6    |15 |  Data bus bit 6                                         |
 |   D5    |16 |  Data bus bit 5                                         |
 |   D4    |17 |  Data bus bit 4                                         |
 |   D3    |18 |  Data bus bit 3                                         |
 |   D2    |19 |  Data bus bit 2                                         |
 |   D1    |20 |  Data bus bit 1                                         |
 |   D0    |21 |  Data bus bit 0                                         |
 |   GND   |22 |  System ground                                          |
 |   GND   | A |                                                         |
 |  ROMH*  | B |  8K decoded RAM/ROM block @ $E000 buffered              |
 |  RESET* | C |  6502 RESET pin(active low) buff'ed ttl out/unbuff'ed in|
 |  NMI*   | D |  6502 Non Maskable Interrupt (active low) buff'ed ttl out, unbuff'ed in |
 |   02    | E |  Phase 2 system clock                                   |
 |   A15   | F |  Address bus bit 15   unbuffered, 1 ls ttl load max     |
 |   A14   | H |  Address bus bit 14                                     |
 |   A13   | J |  Address bus bit 13                                     |
 |   A12   | K |  Address bus bit 12                                     |
 |   A11   | L |  Address bus bit 11                                     |
 |   A10   | M |  Address bus bit 10                                     |
 |   A9    | N |  Address bus bit 9                                      |
 |   A8    | P |  Address bus bit 8                                      |
 |   A7    | R |  Address bus bit 7                                      |
 |   A6    | S |  Address bus bit 6                                      |
 |   A5    | T |  Address bus bit 5                                      |
 |   A4    | U |  Address bus bit 4                                      |
 |   A3    | V |  Address bus bit 3                                      |
 |   A2    | W |  Address bus bit 2                                      |
 |   A1    | X |  Address bus bit 1                                      |
 |   A0    | Y |  Address bus bit 0                                      |
 |   GND   | Z |  System ground                                          |

 \* Active low
 <span epub:type="pagebreak" id="p368" class="pagenumber">368</span>


 Following is a description of some important fines on the expansion
 port:

 **Pins 1,22,A,Z** are connected to the system ground.

 **Pin 6** is the DOT CLOCK. This is the 8.18-MHz video dot clock. All
   system timing is derived from this clock.

 **Pin 12** is the BA (BUS AVAILABLE) signal from the VIC-II
   chip. This line will go low 3 cycles before the VIC-II takes over
   the system busses, and remains low until the VIC-II is finished
   fetching display information.

 **Pin 13** is the DMA (DIRECT MEMORY ACCESS) line. When this line is
   pulled low, the address bus, the data bus, and the Read/Write line
   of the 6510 processor chip enter high-impedance state mode. This
   allows an external processor to take control of the system
   busses. This line should only be pulled low when the (02 clock is
   low. Also, since the VIC-II chip will continue to perform display
   DMA, the external device must conform to the VIC-II timing. (See
   VIC-II timing diagram.) This line is pulled up on the Commodore 64.
 <a name="06-9">





## Z-80 Microprocessor Cartridge

 Reading this book and using your computer has shown you just how
 versatile your Commodore 64 really is. But what makes this machine
 even more capable of meeting your needs is the addition of peripheral
 equipment. Peripherals are things like Datassette™ recorders, disk
 drives, printers, and modems. All these items can be added to your
 Commodore 64 through the various ports and sockets on the back of
 your machine. The thing that makes Commodore peripherals so good is
 the fact that our peripherals are “intelligent.” That means that they
 don't take up valuable Random Access Memory space when they're in
 use. You're free to use all 64K of memory in your Commodore 64.

 Another advantage of your Commodore 64 is the fact most programs you
 write on your Commodore 64 today will be upwardly compatible with any
 new Commodore computer you buy in the future. This is partially
 because of the qualities of the computer's Operating System (OS).

 However, there is one thing that the Commodore OS can't do: make your
 programs compatible with a computer made by another company.
 <span epub:type="pagebreak" id="p369" class="pagenumber">369</span>


 Most of the time you won't even have to think about using another
 company's computer, because your Commodore 64 is so easy to use. But
 for the occasional user who wants to take advantage of software that
 may not be available in Commodore 64 format we have created a
 Commodore CP/M® cartridge.

 CP/M® is not a “computer dependent” operating system. Instead it uses
 some of the memory space normally available for programming to run
 its own operating system. There are advantages and disadvantages to
 this. The disadvantages are that the programs you write will have to
 be shorter than the programs you can write using the Commodore 64's
 built-in operating system. In addition, you can NOT use the Commodore
 64's powerful screen editing capabilities. The advantages are that
 you can now use a large amount of software that has been specifically
 designed for CP/M® and the Z-80 microprocessor, and the programs that
 you write using the CP/M® operating system can be transported and run
 on any other computer that has CP/M® and a Z-80 card.

 By the way, most computers that have a Z-80 microprocessor require
 that you go inside the computer to actually install a Z-80 card. With
 this method you have to be very careful not to disturb the delicate
 circuitry that runs the rest of the computer. The Commodore CP/M®
 cartridge eliminates this hassle because our Z-80 cartridge plugs
 into the back of your Commodore 64 quickly and easily, without any
 messy wires that can cause problems later.




### Using Commodore CP/M®

 The Commodore Z-80 cartridge let's you run programs designed for a
 Z-80 microprocessor on your Commodore 64. The cartridge is provided
 with a diskette containing the Commodore CP/M® operating system.




### Running Commodore CP/M®

 To run CP/M®:

 1. LOAD the CP/M® program from your disk drive.
 2. Type RUN.
 3. Hit the <kbd>RETURN</kbd> key.


 <span epub:type="pagebreak" id="p370" class="pagenumber">370</span>

 At this point the 64K bytes of RAM in the Commodore 64 are accessible
 by the built-in 6510 central processor, OR 48K bytes of RAM are
 available for the Z-80 central processor. You can shift back and
 forth between these two processors, but you can NOT use them at the
 same time in a single program. This is possible because of your
 Commodore 64's sophisticated timing mechanism.

 Below is the memory address translation that is performed on the Z-80
 cartridge. You should notice that by adding 4096 bytes to the memory
 locations used in CP/M® $1000 (hex) you equal the memory addresses of
 the normal Commodore 64 operating system. The correspondence between
 Z-80 and 6510 memory addresses is as follows:


 |    Z-80 DEC     |    Z-80 HEX     |     6510 DEC    |    6510 HEX     |
 | --------------- | --------------- | --------------- | --------------- |
 |    0000-4095    |    0000-0FFF    |    4096-8191    |    1000-1FFF    |
 |    4096-8191    |    1000-1FFF    |    8192-12287   |    2000-2FFF    |
 |    8192-12287   |    2000-2FFF    |   12288-16383   |    3000-3FFF    |
 |   12288-16383   |    3000-3FFF    |   16384-20479   |    4000-4FFF    |
 |   16384-20479   |    4000-4FFF    |   20480-24575   |    5000-5FFF    |
 |   20480-24575   |    5000-5FFF    |   24576-28671   |    6000-6FFF    |
 |   24576-28671   |    6000-6FFF    |   28672-32767   |    7000-7FFF    |
 |   28672-32767   |    7000-7FFF    |   32768-36863   |    8000-SFFF    |
 |   32768-36863   |    8000-8FFF    |   36864-40959   |    9000-9FFF    |
 |   36864-40959   |    9000-9FFF    |   40960-45055   |    A000-AFFF    |
 |   40960-45055   |    A000-AFFF    |   45056-49151   |    B000-BFFF    |
 |   45056-49151   |    B000-BFFF    |   49152-53247   |    C000-CFFF    |
 |   49152-53247   |    C000-CFFF    |   53248-57343   |    D000-DFFF    |
 |   53248-57343   |    D000-DFFF    |   57344-61439   |    E000-EFFF    |
 |   57344-61439   |    E000-EFFF    |   61440-65535   |    F000-FFFF    |
 |   61440-65535   |    F000-FFFF    |    0000-4095    |    0000-0FFF    |

 <span epub:type="pagebreak" id="p371" class="pagenumber">371</span>

 To TURN ON the Z-80 and TURN OFF the 6510 chip, type in the following
 program:
 [06-371-1.bas](code/06-371-1.bas)


``` basic 06-371-1.bas
10 REM THIS PROGRAM IS TO BE USED WITH THE Z80 CARD
20 REM IT FIRST STORES Z80 DATA AT $1000 (Z80=$0000)
30 REM THEN IT TURNS OFF THE 6510 IRQ'S AND ENABLES
40 REM THE Z80 CARD. THE Z80 CARD MUST BE TURNED OFF
50 REM TO REENABLE THE 6510 SYSTEM.
100 REM STORE Z80 DATA
110 READ B :REM GET SIZE OF Z80 CODE TO BE MOVED
120 FOR I=4096 TO 4096+B-1 :REM MOVE CODE
130 READ A :POKE I,A
140 NEXT I
200 REM RUN Z80 CODE
210 POKE 56333,127 :REM TURN OF 6510 IRQ'S
220 POKE 56832,00  :REM TURN ON Z80 CARD
230 POKE 56333,129 :REM TURN ON 6510 IRQ'S WHEN Z80 DONE
240 END
1000 REM Z80 MACHINE LANGUAGE CODE DATA SECTION
1010 DATA 18 :REM SIZE OF DATA TO BE PASSED
1100 REM Z80 TURN ON CODE
1110 DATA 00,00,00 :REM OUR Z80 CARD REQUIRES TURN ON TIME AT $0000
1200 REM Z80 TASK DATA HERE
1210 DATA 33,02,245 :REM LD HL,NN (LOCATION ON SCREEN)
1220 DATA 52 :REM INC HL (INCREMENT THAT LOCATION)
1300 REM Z80 SELF-TURN OFF DATA HERE
1310 DATA 62,01 :REM LD A,N
1320 DATA 50,00,206 :REM LD (NN),A :I/O LOCATION
1330 DATA 00,00,00  :REM NOP, NOP, NOP
1340 DATA 195,00,00 :REM JMP $0000
```

 For more details about Commodore CP/M® and the Z-80 microprocessor
 look for the cartridge and the Z-80 Reference Guide at your local
 Commodore computer dealer.
 <span epub:type="pagebreak" id="p372" class="pagenumber">372</span>
