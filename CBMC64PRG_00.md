<span epub:type="pagebreak" id="pi" class="pagenumber">i</span>
<a name="top">




#  Commodore 64 Programmer’s Reference Guide

 Published by Commodore Business Machines, Inc.  and Howard W. Sams &
 Co., Inc.

<span epub:type="pagebreak" id="pii" class="pagenumber">ii</span>

 FIRST EDITION FOURTH PRINTING-1983

 Copyright (C) 1982 by Commodore Business Machines, Inc.  All rights reserved.

 This manual is copyrighted and contains proprietary information. No
 part of this publication may be reproduced, stored in a retrieval
 system, or transmitted in any form or by any means, electronic,
 mechanical, photocopying, recording, or otherwise, without the
 prior written permission of COMMODORE BUSINESS MACHINES, Inc.
 <span epub:type="pagebreak" id="piii" class="pagenumber">iii</span>




## Table of Contents


 * [Introduction …                                           **ix**](#pix)
   * [What’s Included? …                                      **x**](#px)
   * [How to Use This Reference Guide …                      **xi**](#pxi)
   * [Commodore 64 Applications Guide …                     **xii**](#pxii)
   * [Commodore Information Network …                      **xvii**](#pxvii)

 1. [BASIC Programming Rules …                                **1**](#p1)
   * [Introduction …                                          **2**](#p2)
   * [Screen Display Codes (BASIC Character Set) The Operating System (OS) … **2**](#2)
   * [Programming Numbers and Variables …                     **4**](#p4)
      * [Integer, Floating-Point and String Constants …       **4**](#p4)
      * [Integer, Floating-Point and String Variables …       **7**](#p7)
      * [Integer, Floating-Point and String Arrays …          **8**](#p8)
   * [Expressions and Operators …                             **9**](#p9)
      * [Arithmetic Expressions …                            **10**](#p10)
      * [Arithmetic Operations …                             **10**](#p10)
      * [Relational Operators …                              **12**](#p12)
      * [Logical Operators …                                 **13**](#p13)
      * [Hierarchy of Operations …                           **15**](#p15)
      * [String Operations …                                 **16**](#p16)
      * [String Expressions …                                **17**](#p17)
   * [Programming Techniques …                               **18**](#p18)
      * [Data Conversions …                                  **18**](#p18)
      * [Using the INPUT Statement …                         **18**](#p18)
      * [Using the GET Statement …                           **22**](#p22)
      * [How to Crunch BASIC Programs …                      **24**](#p24)

 2. [BASIC Language Vocabulary …                             **29**](#p29)
   * [Introduction …                                         **30**](#p30)
   * [BASIC Keywords, Abbreviations, and Function Types …    **31**](#p31)
   * [Description of BASIC Keywords (Alphabetical) …         **35**](#p35)
   * [The Commodore 64 Keyboard and Features …               **93**](#p93)
   * [Screen Editor …                                        **94**](#p94)
 <span epub:type="pagebreak" id="piv" class="pagenumber">iv</span>

 3. [Programming Graphics on the Commodore 64 …              **99**](#p99)
   * [Graphics Overview …                                   **100**](#p100)
      * [Character Display Modes …                          **100**](#p100)
      * [Bit Map Modes …                                    **100**](#p100)
      * [Sprites …                                          **100**](#p100)
    * [Graphics locations …                                 **101**](#p101)
      * [Video Bank Selection …                             **101**](#p101)
      * [Screen Memory …                                    **102**](#p102)
      * [Color Memory …                                     **103**](#p103)
      * [Character Memory …                                 **103**](#p103)
    * [Standard Character Mode …                            **107**](#p107)
      * [Character Definitions …                            **107**](#p107)
    * [Programmable Characters …                            **108**](#p108)
    * [Multi-Color Mode Graphics …                          **115**](#p115)
      * [Multi-Color Mode Bit …                             **115**](#p115)
    * [Extended Background Color Mode …                     **120**](#p120)
    * [Bit Mapped Graphics …                                **121**](#p121)
      * [Standard High-Resolution Bit Map Mode …            **122**](#p122)
      * [How It Works …                                     **122**](#p122)
    * [Multi-Color Bit Map Mode …                           **127**](#p127)
    * [Smooth Scrolling …                                   **128**](#p128)
    * [Sprites …                                            **131**](#p131)
      * [Defining a Sprite …                                **131**](#p131)
      * [Sprite Pointers …                                  **133**](#p133)
      * [Turning Sprites On …                               **134**](#p134)
      * [Turning Sprites Off …                              **135**](#p135)
      * [Colors …                                           **135**](#p135)
      * [Multi-Color Mode …                                 **135**](#p135)
      * [Setting a Sprite to Multi-Color Mode …             **136**](#p136)
      * [Expanded Sprites …                                 **136**](#p136)
      * [Sprite Positioning …                               **137**](#p137)
      * [Sprite Positioning Summary …                       **143**](#p143)
      * [Sprite Display Priorities …                        **144**](#p144)
      * [Collision Detects …                                **144**](#p144)
    * [Other Graphics Features …                            **150**](#p150)
      * [Screen Blanking …                                  **150**](#p150)
      * [Raster Register …                                  **150**](#p150)
      * [Interrupt Status Register …                        **151**](#p151)
      * [Suggested Screen and Character Color Combinations … **152**](#p152)
 <span epub:type="pagebreak" id="pv" class="pagenumber">v</span>
    * [Programming Sprites-Another Look …                   **153**](#p100)
      * [Making Sprites in BASIC-A Short Program …          **153**](#p100)
      * [Crunching Your Sprite Programs …                   **156**](#p100)
      * [Positioning Sprites on the Screen …                **157**](#p100)
      * [Sprite Priorities …                                **161**](#p100)
      * [Drawing a Sprite …                                 **162**](#p100)
      * [Creating a Sprite…Step by Step …                   **163**](#p100)
      * [Moving Your Sprite on the Screen …                 **165**](#p100)
      * [Vertical Scrolling …                               **166**](#p100)
      * [The Dancing Mouse-A Sprite Program Example …       **166**](#p100)
      * [Easy Spritemaking Chart …                          **176**](#p100)
      * [Spritemaking Notes …                               **177**](#p100)

 4. [Programming Sound and Music on Your Commodore 64 …     **183**](#p100)
    * [Introduction …                                       **184**](#p100)
      * [Volume Control …                                   **186**](#p100)
      * [Frequencies of Sound Waves …                       **186**](#p100)
    * [Using Multiple Voices …                              **187**](#p100)
      * [Controlling Multiple Voices …                      **191**](#p100)
    * [Changing Waveforms …                                 **192**](#p100)
      * [Understanding Waveforms …                          **194**](#p100)
    * [The Envelope Generator …                             **196**](#p100)
    * [Filtering …                                          **199**](#p100)
    * [Advanced Techniques …                                **202**](#p100)
    * [Synchronization and Ring Modulation …                **207**](#p100)

 5. [BASIC to Machine Language …                            **209**](#p100)
    * [What is Machine Language? …                          **210**](#p100)
      * [What Does Machine Code Look Like? …                **211**](#p100)
      * [Simple Memory Map of the Commodore 64 …            **212**](#p100)
      * [The Registers Inside the 6510 Microprocessor …     **213**](#p100)
    * [How Do You Write Machine Language Programs? …        **214**](#p100)
      * [64MON …                                            **215**](#p100)
    * [Hexadecimal Notation …                               **215**](#p100)
      * [Your First Machine Language Instruction …          **218**](#p100)
      * [Writing Your First Program …                       **220**](#p100)
    * [Addressing Modes …                                   **221**](#p100)
      * [Zero Page …                                        **221**](#p100)
      * [The Stack …                                        **222**](#p100)
 <span epub:type="pagebreak" id="pvi" class="pagenumber">vi</span>
   * [Indexing …                                            **223**](#p100)
     * [Indirect Indexed …                                  **223**](#p100)
     * [Indexed Indirect …                                  **224**](#p100)
     * [Branches and Testing …                              **226**](#p100)
   * [Subroutines …                                         **228**](#p100)
   * [Useful Tips for the Beginner …                        **229**](#p100)
   * [Approaching a Large Task …                            **230**](#p100)
   * [MCS6510 Microprocessor Instruction Set-Alphabetic Sequence … **232**](#p100)
     * [Instruction Addressing Modes and Related Execution Times … **254**](#p100)
   * [Memory Management on the Commodore 64 …               **260**](#p100)
   * [The KERNAL …                                          **268**](#p100)
   * [KERNAL Power-Up Activities …                          **269**](#p100)
     * [How to Use the KERNAL …                             **270**](#p100)
     * [User Callable KERNAL Routines …                     **272**](#p100)
     * [Error Codes …                                       **306**](#p100)
   * [Using Machine Language From BASIC …                   **307**](#p100)
     * [Where to Put Machine Language Routines …            **309**](#p100)
     * [How to Enter Machine language …                     **309**](#p100)
   * [Commodore 64 Memory Map …                             **310**](#p100)
     * [Commodore 64 Input/Output Assignments …             **320**](#p100)

 6. [Input/Output Guide …                                   **335**](#p100)
   * [Introduction …                                        **336**](#p100)
   * [Output to the TV …                                    **336**](#p100)
   * [Output to Other Devices …                             **337**](#p100)
     * [Output to Printer …                                 **338**](#p100)
     * [Output to Modem …                                   **339**](#p100)
     * [Working With Cassette Tape …                        **340**](#p100)
     * [Data Storage on Floppy Diskettes …                  **342**](#p100)
   * [The Game Ports …                                      **343**](#p100)
     * [Paddles …                                           **346**](#p100)
     * [Light Pen …                                         **348**](#p100)
   * [RS-232 Interface Description …                        **348**](#p100)
     * [General Outline …                                   **348**](#p100)
     * [Opening an RS-232 Channel …                         **349**](#p100)
     * [Getting Data From an RS-232 Channel …               **352**](#p100)
     * [Sending Data to an RS-232 Channel …                 **353**](#p100)
     * [Closing an RS-232 Data Channel …                    **354**](#p100)
     * [Sample BASIC Programs …                             **356**](#p100)
 <span epub:type="pagebreak" id="pvii" class="pagenumber">vii</span>
     * [Receiver/Transmitter Buffer Base Location Pointers … **357**]()
     * [Zero-Page Memory Locations and Usage for RS-232 System Interface … **358**]()
     * [Nonzero-Page Memory Locations and Usage for RS-232 System Interface … **358**]()
   * [The User Port …                                             **359**](#p100)
     * [Port Pin Description …                                    **359**](#p100)
   * [The Serial Bus …                                            **362**](#p100)
     * [Serial Bus Pinouts …                                      **363**](#p100)
   * [The Expansion Port …                                        **366**](#p100)
   * [Z-80 Microprocessor Cartridge …                             **368**](#p100)
     * [Using Commodore CP/M® …                                **369**](#p100)
     * [Running Commodore CP/M® …                              **369**](#p100)

 * [Appendices …                                                  **373**](#p100)
   * [A. Abbreviations for BASIC Keywords …                         **374**](#p100)
   * [B. Screen Display Codes …                                     **376**](#p100)
   * [C. ASCII and CHR$ Codes …                                     **379**](#p100)
   * [D. Screen and Color Memory Maps …                             **382**](#p100)
   * [E. Music Note Values …                                        **384**](#p100)
   * [F. Bibliography …                                             **388**](#p100)
   * [G. VIC Chip Register Map …                                    **391**](#p100)
   * [H. Deriving Mathematical Functions …                          **394**](#p100)
   * [I. Pinouts for Input/Output Devices …                         **395**](#p100)
   * [J. Converting Standard BASIC Programs to Commodore 64 BASIC … **398**]()
   * [K. Error Messages …                                           **400**](#p100)
   * [L. 6510 Microprocessor Chip Specifications …                  **402**](#p100)
   * [M. 6526 Complex Interface Adapter (CIA) Chip Specifications … **419**]()
   * [N. 6566/6567 (VIC-II) Chip Specifications …                   **436**](#p100)
   * [O. 6581 Sound Interface Device (SID) Chip Specifications …    **457**](#p100)
   * [P. Glossary …                                                 **482**](#p100)

 * [INDEX …                                                       **483**](#p100)

 * [Commodore 64 Quick Reference Card …                           **487**](#p100)

 * [Schematic Diagram of the Commodore 64 …                       **491**](#p100)
 <span epub:type="pagebreak" id="pix" class="pagenumber">ix</span>




##  Introduction

 The COMMODORE 64 PROGRAMMER’S REFERENCE GUIDE has been developed as a
 working tool and reference source for those of you who want to
 maximize your use of the built-in capabilities of your COMMODORE 64.
 This manual contains the information you need for your programs,
 from the simplest example all the way to the most complex. The
 PROGRAMMER’S REFERENCE GUIDE is designed so that everyone from the
 beginning BASIC programmer to the professional experienced in 6502
 machine language can get information to develop his or her own
 creative programs. At the same time this book shows you how clever
 your COMMODORE 64 really is.

 This REFERENCE GUIDE is not designed to teach the BASIC programming
 language or the 6502 machine language. There is, however, an
 extensive glossary of terms and a “semi-tutorial” approach to many of
 the sections in the book. If you don’t already have a working
 knowledge of BASIC and how to use it to program, we suggest that you
 study the COMMODORE 64 USER’S GUIDE that came with your computer. The
 USER’S GUIDE gives you an easy to read introduction to the BASIC
 programming language. If you still have difficulty understanding how
 to use BASIC then turn to the back of this book (or Appendix N in the
 USER’S GUIDE) and check out the Bibliography.

 The COMMODORE 64 PROGRAMMER’S REFERENCE GUIDE is just that; a
 reference. Like most reference books, your ability to apply the
 information creatively really depends on how much knowledge you have
 about the subject. In other words if you are a novice programmer you
 will not be able to use all the facts and figures in this book until
 you expand your current programming knowledge.
 <span epub:type="pagebreak" id="px" class="pagenumber">x</span>

 What you can do with this book is to find a considerable amount of
 valuable programming reference information written in easy to read,
 plain English with the programmer’s jargon explained. On the other
 hand the programming professional will find all the information
 needed to use the capabilities of the COMMODORE 64 effectively.


## What’s Included?

 * Our complete “BASIC dictionary” includes Commodore BASIC language
   commands, statements and functions listed in alphabetical order.
   We’ve created a “quick list” which contains all the words and their
   abbreviations. This is followed by a section containing a more
   detailed definition of each word along with sample BASIC programs
   to illustrate how they work.

 * If you need an introduction to using machine language with BASIC
   programs our layman’s overview will get you started.

 * A powerful feature of all Commodore computers is called the KERNAL.
   It helps insure that the programs you write today can also be used
   on your Commodore computer of tomorrow.

 * The Input/Output Programming section gives you the opportunity to
   use your computer to the limit. It describes how to hook-up and use
   everything from lightpens and joysticks to disk drives, printers,
   and telecommunication devices called modems.

 * You can explore the world of SPRITES, programmable characters, and
   high resolution graphics for the most detailed and advanced
   animated pictures in the microcomputer industry.

 * You can also enter the world of music synthesis and create your own
   songs and sound effects with the best built-in synthesizer
   available in any personal computer.

 * If you’re an experienced programmer, the soft load language section
   gives you information about the COMMODORE 64’s ability to run CP/M*
   and high level languages. This is in addition to BASIC.


 Think of your COMMODORE 64 PROGRAMMER’S REFERENCE GUIDE as a useful
 tool to help you and you will enjoy the -hours of programming ahead
 of you.

 <small>* CP/M is a registered trademark of Digital Research, Inc.</small>
 <span epub:type="pagebreak" id="pxi" class="pagenumber">xi</span>




## How to Use This Reference Guide

 Throughout this manual certain conventional notations are used to
 describe the syntax (programming sentence structure) of BASIC
 commands or statements and to show both the required and optional
 parts of each BASIC keyword. The rules to use for interpreting
 statement syntax are as follows:

 1. BASIC keywords are shown in capital letters. They must appear
 where shown in the statement, entered and spelled exactly as shown.

 2. Items shown within quotation marks (" ") indicate variable data
 which you must put in. Both the quotation marks and the data inside
 the quotes must appear where shown in each statement.

 3. Items inside the square brackets ([ ]) indicate an optional
 statement parameter. A parameter is a limitation or additional
 qualifier for your statements. If you use an optional parameter you
 must supply the data for that optional parameter. In addition,
 ellipses (…) show that an optional item can be repeated as many times
 as a programming line allows.

 4. If an item in the square brackets ([ ]) is UNDERLINED, that means
 that you MUST use those certain characters in the optional
 parameters, and they also have to be spelled exactly as shown.

 5. Items inside angle brackets (< >) indicate variable data which you
 provide. While the slash (/) indicates that you must make a choice
 between two mutually exclusive options.




## Example of Syntax Format:

```OPEN <file-num>,<device>[,<address>],["<drive>:<filename>][,<mode>]"```



## Examples of Actual Statements:

``` basic
10 OPEN 2,8,6,"0:STOCK FOLIO,S,W"
20 OPEN 1,1,2,"CHECKBOOK"
30 OPEN 3,4
```

 When you actually apply the syntax conventions in a practical
 situation, the sequence of parameters in your statements might not be
 exactly the same as the sequence shown in syntax examples. The
 examples are not meant to show every possible sequence. They are
 intended to present all required and optional parameters.
 <span epub:type="pagebreak" id="pxii" class="pagenumber">xii</span>

 Programming examples in this book are shown with blanks separating
 words and operators for the sake of readability. Normally though,
 BASIC doesn’t require blanks between words unless leaving them out
 would give you an ambiguous or incorrect syntax.

 Shown below are some examples and descriptions of the symbols used
 for various statement parameters in the following chapters. The list
 is not meant to show every possibility, but to give you a better
 understanding as to how syntax examples are presented.


 |     Symbol     |   Example   |              Description                 |
 |:-------------- | -----------:|:---------------------------------------- |
 | \<file-num\>   | 50          | A logical file number                    |
 | \<device\>     | 4           | A hardware device number                 |
 | \<address\>    | 15          | A serial bus secondary device address number |
 | \<drive\>      | 0           | A physical disk drive number             |
 | \<file-name\>  | "TEST.DATA" | The name of a data or program file       |
 | \<constant\>   | "ABCDEFG"   | Literal data supplied by the programmer  |
 | \<variable\>   | X145        | Any BASIC data variable name or constant |
 | \<string\>     | AB$         | Use of a string type variable required   |
 | \<number\>     | 12345       | Use of a numeric type variable required  |
 | \<line-number\>| 1000        | An actual program line number            |
 | \<numeric\>    | 1.5E4       | An integer or floating-point variable    |




## Commodore 64 Applications Guide

 When you first thought about buying a computer you probably asked
 yourself, "Now that I can afford to buy a computer, what can I do
 with it once I get one?"

 The great thing about your COMMODORE 64 is that you can make it do
 what YOU want it to do! You can make it calculate and keep track of
 home and business budget needs. You can use it for word
 processing. You can make it play arcade-style action games. You can
 make it sing. You can even create your own animated cartoons, and
 more. The best part of owning a COMMODORE 64 is that even if it did
 only one of the things listed below it would be well worth the price
 you paid for it. But the 64 is a complete computer and it does do
 EVERYTHING listed and then some!
 <span epub:type="pagebreak" id="pxiii" class="pagenumber">xiii</span>

 By the way, in addition to everything here you can pick up a lot of
 other creative and practical ideas by signing up with a local
 Commodore Users’ Club, subscribing to the COMMODORE and POWER/PLAY
 magazines, and joining the COMMODORE INFORMATION NETWORK on
 CompuServe™


 **APPLICATION** COMMENTS/REQUIREMENTS

 **ACTION PACKED** You can get real Bally Midway arcade games GAMES
   like Omega Race, Gorf and Wizard of War, as well as “play and
   learn” games like Math Teacher 1, Home Babysitter and Commodore
   Artist.

 **ADVERTISING & MERCHANDISING** Hook your COMMODORE 64 to a TV, put
   it in a store window with a flashing, animated, and musical message
   and you’ve got a great point of purchase store display.

 **ANIMATION** Commodore’s Sprite Graphics allow you to create real
   cartoons with 8 different levels so that shapes can move in front
   of or behind each other.

 **BABYSITTING** The COMMODORE 64 HOME BABYSITTER cartridge can keep
   your youngest child occupied for hours and teach alphabet/keyboard
   recognition at the same time. It also teaches special learning
   concepts and relationships.

 **BASIC PROGRAMMING** Your COMMODORE 64 USER’S GUIDE and the TEACH
   YOURSELF PROGRAMMING series of books and tapes offer an excellent
   starting point.

 **BUSINESS SPREADSHEET** The COMMODORE 64 offers the “Easy” series of
   business aids including the most powerful word processor and
   largest spreadsheet available for any personal computer.

 **COMMUNICATION** Enter the fascinating world of computer
   “networking.” If you hook a VICMODEM to your COMMODORE 64 you can
   communicate with other computer owners all around the world.
 <span epub:type="pagebreak" id="pxiv" class="pagenumber">xiv</span>
   Not only that, if you join the COMMODORE INFORMATION NETWORK on
   CompuServe™ you can get the latest news and updates on all
   Commodore products, financial information, shop at home services,
   you can even play games with the friends you make through the
   information systems you join.

 **COMPOSING SONGS** The COMMODORE 64 is equipped with the most
   sophisticated built-in music synthesizer available on any
   computer. It has three completely programmable voices, nine full
   music octaves, and four controllable waveforms. Look for Commodore
   Music Cartridges and Commodore Music books to help you create or
   reproduce all kinds of music and sound effects.

 **CP/M\*** Commodore offers a CP/M* add-on and access to software
   through an easy-to-load cartridge.

 **DEXTERITY TRAINING** Hand/Eye coordination and manual dexterity are
   aided by several Commodore games… including “Jupiter lander” and
   night driving simulation.

 **EDUCATION** While working with a computer is an education in
   itself, The COMMODORE Educational Resource Book contains general
   information on the educational uses of computers. We also have a
   variety of learning cartridges designed to teach everything from
   music to math and art to astronomy.

 **FOREIGN LANGUAGE** The COMMODORE 64 programmable character set lets
   you replace the standard character set with user defined foreign
   language characters.

 **GRAPHICS AND ART** In addition to the Sprite Graphics mentioned
   above, the COMMODORE 64 offers high-resolution, multi-color
   graphics plotting, programmable
 <span epub:type="pagebreak" id="pxv" class="pagenumber">xv</span>
   characters, and combinations of all the different graphics and
   character display modes.

 **INSTRUMENT CONTROL** Your COMMODORE 64 has a serial port, RS-232
   port and a user port for use with a variety of special industrial
   applications. An IEEE/488 cartridge is also available as an
   optional extra.

 **JOURNALS AND CREATIVE WRITING** The COMMODORE 64 will soon offer an
   exceptional wordprocessing system that matches or exceeds the
   qualities and flexibilities of most “high-priced” wordprocessors
   available. Of course you can save the information on either a 1541
   Disk Drive or a Datassette™ recorder and have it printed out
   using a VIC-PRINTER or PLOTTER.

 **LIGHTPEN CONTROL** Applications requiring the use of a lightpen can
   be performed by any lightpen that will fit the COMMODORE 64 game
   port connector.

 **MACHINE CODE PROGRAMMING** Your COMMODORE 64 PROGRAMMER’S REFERENCE
   GUIDE includes a machine language section, as well as a BASIC to
   machine code interface section. There’s even a bibliography
   available for more in-depth study.

 **PAYROLL & FORMS PRINTOUT** The COMMODORE 64 can be programmed to
   handle a variety of entry-type business applications. Upper/lower
   case letters combined with C64 “business form” graphics make it
   easy for you to design forms which can then be printed on your
   printer.

 **PRINTING** The COMMODORE 64 interfaces with a variety of dot matrix
   and letter quality printers as well as plotters.

 **RECIPES** You can store your favorite recipes on your COMMODORE 64
   and its disk or cassette storage unit, and end the need for messy
   recipe cards that often get lost when you need them most.
 <span epub:type="pagebreak" id="pxiv" class="pagenumber">xiv</span>

 **SIMULATIONS** Computer simulations let you conduct dangerous or
   expensive experiments at minimum risk and cost.

 **SPORTS DATA** The Source™ and CompuServe™ both offer sports
   information which you can get using your COMMODORE 64 and a
   VICMODEM.

 **STOCK QUOTES** With a VICMODEM and a subscription to any of the
   appropriate network services, your COMMODORE 64 becomes your own
   private stock ticker.

 <small>* CP/M is a Registered trademark of Digital Research, Inc.</small>


 These are just a few of the many applications for you and your
 COMMODORE 64. As you can see, for work or play, at home, in school or
 the office, your COMMODORE 64 gives you a practical solution for just
 about any need.

 Commodore wants you to know that our support for users only STARTS
 with your purchase of a Commodore computer. That’s why we’ve created
 two publications with Commodore information from around the world,
 and a “two-way” computer information network with valuable input for
 users in the U.S. and Canada from coast to coast.

 In addition, we wholeheartedly encourage and support the growth of
 Commodore Users’ Clubs around the world. They are an excellent source
 of information for every Commodore computer owner from the beginner
 to the most advanced. The magazines and network, which are more fully
 described below, have the most up-to-date information about how to
 get involved with the Users’ Club in your area.

 Finally, your local Commodore dealer is a useful source of Commodore
 support and information.




### POWER/PLAY - The Home Computer Magazine

 When it comes to entertainment, learning at home and practical home
 applications, POWER/PLAY is THE prime source of information for
 Commodore home users. Find out where your nearest user clubs are and
 what they’re doing, learn about software, games, programming
 techniques, telecommunications, and new products. POWER/PLAY is your
 personal connection to other Commodore users, outside software and
 hardware developers, and to Commodore itself. Published
 quarterly. Only $10.00 for a year of home computing excitement.
 <span epub:type="pagebreak" id="pxvii" class="pagenumber">xvii</span>




### COMMODORE - The Microcomputer Magazine

 Widely read by educators, businessmen and students, as well as home
 computerists, COMMODORE Magazine is our main vehicle for sharing
 exclusive information on the more technical use of Commodore systems.
 Regular departments cover business, science and education,
 programming tips, “excerpts from a technical notebook,” and many
 other features of interest to anyone who uses or is thinking about
 purchasing Commodore equipment for business, scientific or
 educational applications.  COMMODORE is the ideal complement to
 POWER/PLAY. Published bimonthly.  Subscription price: $15.00 per
 year.




### And for Even More Information…

 …DIAL UP OUR PAPERLESS USER MAGAZINE
  COMMODORE INFORMATION NETWORK

 The magazine of the future is here. To supplement and enhance your
 subscription to POWER/PLAY and COMMODORE magazines, the COMMODORE
 INFORMATION NETWORK—our “paperless magazine”—is available now
 over the telephone using your Commodore computer and modem.

 Join our computer club, get help with a computing problem, “talk” to
 other Commodore friends, or get up-to-the-minute information on new
 products, software and educational resources. Soon you will even be
 able to save yourself the trouble of typing in the program listings
 you find in POWER/PLAY or COMMODORE by downloading direct from the
 Information Network (a new user service planned for early 1983). The
 best part is that most of the answers are there before you even ask
 the questions. (How’s that for service?)

 To call our electronic magazine you need only a modem and a
 subscription to CompuServe™, one of the nation’s largest
 telecommunications networks. (To make it easy for you Commodore
 includes a FREE year’s subscription to CompuServe™ in each VICMODEM
 package.) Just dial your local number for the CompuServe™ data
 bank and connect your phone to the modem. When the CompuServe™
 video text appears on your screen type G CBM on your computer
 keyboard. When the COMMODORE INFORMATION NETWORK’S table of contents,
 or “menu,” appears on your screen choose from one of our sixteen
 departments, make yourself comfortable, and enjoy the paperless
 magazine other magazines are writing about.
 <span epub:type="pagebreak" id="pxviii" class="pagenumber">xviii</span>

 For more information, visit your Commodore dealer or contact
 Compuserve™ customer service at 800-848-8990 (in Ohio, 614-457-8600).

## Commodore Information Network

 * Main Menu Description
 * Direct Access Codes
 * Special Commands
 * User Questions
 * Public Bulletin Board
 * Magazines and Newsletters
 * Products Announced
 * Commodore News Direct
 * Commodore Dealers
 * Educational Resources
 * User Groups
 * Descriptions
 * Questions and Answers
 * Software Tips
 * Technical Tips
 * Directory Descriptions

