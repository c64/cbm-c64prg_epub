10 REM * EXAMPLE 1 *
20 REM CREATING PROGRAMMABLE CHARACTERS
31 POKE 56334,PEEK(56334)AND 254 :REM TURN OFF KB
32 POKE 1,PEEK(1)AND 251 :REM TURN OFF I/O
35 FOR I=0 TO 63 :REM CHARACTER RANGE TO BE COPIED
36 FOR J=0 TO 7 :REM COPY ALL 8 BYTES PER CHARACTER
37 POKE 12288+I*8+J,PEEK(53248+I*8+J) :REM COPY A BYTE
38 NEXT J :NEXT I :REM GOTO NEXT BYTE OR CHARACTER
39 POKE 1,PEEK(1)OR 4 :POKE 56334,PEEK(56334)OR 1 :REM TURN ON I/O AND KB
40 POKE 53272,(PEEK(53272)AND 240)+12 :REM SET CHAR POINTER TO MEM. 12288
60 FOR CHAR=60 TO 63 :REM PROGRAM CHARACTERS 60 THRU 63
80 FOR BYTE=0 TO 7 :REM DO ALL 8 BYTES OF A CHARACTER
100 READ NUMBER :REM READ IN 1/8TH OF CHARACTER DATA
120 POKE 12288+(8*CHAR)+BYTE,NUMBER :REM STORE THE DATA IN MEMORY
140 NEXT BYTE :NEXT CHAR :REM ALSO COULD BE NEXT BYTE, CHAR
150 PRINT CHR$(147)TAB(255)CHR$(60);
155 PRINT CHR$(61)TAB(55)CHR$(62)CHR$(63)
160 REM LINE 150 PUTS THE NEWLY DEFINED CHARACTERS ON THE SCREEN
170 GET A$ :REM WAIT FOR USER TO PRESS A KEY
180 IF A$="" THEN GOTO 170 :REM IF NO KEYS WERE PRESSED, TRY AGAIN!
190 POKE 53272,21 :REM RETURN TO NORMAL CHARACTERS
200 DATA 4,6,7,5,7,7,3,3 :REM DATA FOR CHARACTER 60
210 DATA 32,96,224,160,224,224,192,192 :REM DATA FOR CHARACTER 61
220 DATA 7,7,7,31,31,95,143,127 :REM DATA FOR CHARACTER 62
230 DATA 224,224,224,248,248,248,240,224 :REM DATA FOR CHARACTER 63
240 END
