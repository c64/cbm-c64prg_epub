10 REM SPRITE EXAMPLE 2...
20 REM THE HOT AIR BALLOON AGAIN
30 VIC=13*4096 :REM THIS IS WHERE THE VIC REGISTERS BEGIN
35 POKE VIC+21,63 :REM ENABLE SPRITES 0 THRU 5
36 POKE VIC+33,14 :REM SET BACKGROUND COLOR TO LIGHT BLUE
37 POKE VIC+23,3 :REM EXPAND SPRITES 0 AND 1 IN Y
38 POKE VIC+29,3 :REM EXPAND SPRITES 0 AND 1 IN X
40 POKE 2040,192 :REM SET SPRITE 0'S POINTER
50 POKE 2041,193 :REM SET SPRITE 1'S POINTER
60 POKE 2042,192 :REM SET SPRITE 2'S POINTER
70 POKE 2043,193 :REM SET SPRITE 3'S POINTER
80 POKE 2044,192 :REM SET SPRITE 4'S POINTER
90 POKE 2045,193 :REM SET SPRITE 5'S POINTER
100 POKE VIC+4,30 :REM SET SPRITE 2'S X POSITION
110 POKE VIC+5,58 :REM SET SPRITE 2'S Y POSITION
120 POKE VIC+6,65 :REM SET SPRITE 3'S X POSITION
130 POKE VIC+7,58 :REM SET SPRITE 3'S Y POSITION
140 POKE VIC+8,100 :REM SET SPRITE 4'S X POSITION
150 POKE VIC+9,58 :REM SET SPRITE 4'S Y POSITION
160 POKE VIC+10,100 :REM SET SPRITE 5'S X POSITION
170 POKE VIC+11,58 :REM SET SPRITE 5'S Y POSITION
175 PRINT"{WHITE}{CLEAR}"TAB(15)"THIS IS TWO HIRES SPRITES";
176 PRINT TAB(55)"ON TOP OF EACH OTHER"
180 POKE VIC+0,100 :REM SET SPRITE 0'S X POSITION
190 POKE VIC+1,100 :REM SET SPRITE 0'S Y POSITION
200 POKE VIC+2,100 :REM SET SPRITE 1'S X POSITION
210 POKE VIC+3,100 :REM SET SPRITE 1'S Y POSITION
220 POKE VIC+39,1 :REM SET SPRITE 0'S COLOR
230 POKE VIC+41,1 :REM SET SPRITE 2'S COLOR
240 POKE VIC+43,1 :REM SET SPRITE 4'S COLOR
250 POKE VIC+40,6 :REM SET SPRITE 1'S COLOR
260 POKE VIC+42,6 :REM SET SPRITE 3'S COLOR
270 POKE VIC+44,6 :REM SET SPRITE 5'S COLOR
280 FOR X=192 TO 193 :REM THE START OF THE LOOP THAT DEFINES THE SPRITES
290 FOR Y=0 TO 63 :REM BYTE COUNTER WITH SPRITE LOOP
300 READ A :REM READ IN A BYTE
310 POKE X*64+Y,A :REM STORE THE DATA IN SPRITE AREA
320 NEXT Y,X :REM CLOSE LOOPS
330 DX=1 :DY=1
340 X=PEEK(VIC) :REM LOOK AT SPRITE 0'S X POSITION
350 IF Y=50 OR Y=208 THEN DY=-DY :REM IF Y IS ON THE EDGE OF THE...
370 REM SCREEN, THEN REVERSE DELTA Y
380 IF X=24 AND(PEEK(VIC+16)AND1)=0 THEN DX=-DX :REM IF SPRITE IS...
390 REM TOUCHING THE LEFT EDGE, THEN REVERSE IT
400 IF X=40 AND(PEEK(VIC+16)AND1)=1 THEN DX=-DX :REM IF SPRITE IS...
410 REM TOUCHING THE RIGHT EDGE, THEN REVERSE IT
420 IF X=255 AND DX=1 THEN X=-1 :SIDE=3
430 REM SWITCH TO OTHER SIDE OF THE SCREEN
440 IF X=0 AND DX=-1 THEN X=256 :SIDE=0
450 REM SWITCH TO OTHER SIDE OF THE SCREEN
460 X=X+DX :REM ADD DELTA X TO X
470 X=X AND 255 :REM MAKE SURE X IS IN ALLOWED RANGE
480 Y=Y+DY :REM ADD DELTA Y TO Y
485 POKE VIC+16,SIDE
490 POKE VIC,X :REM PUT NEW X VALUE INTO SPRITE 0'S X POSITION
500 POKE VIC+2,X :REM PUT NEW X VALUE INTO SPRITE 1'S X POSITION
510 POKE VIC+1,Y :REM PUT NEW Y VALUE INTO SPRITE 0'S Y POSITION
520 POKE VIC+3,Y :REM PUT NEW Y VALUE INTO SPRITE 1'S Y POSITION
530 GOTO340
600 REM ***** SPRITE DATA *****
610 DATA 0,255,0,3,153,192,7,24,224,7,56,224,14,126,112,14,126,112,14,126
620 DATA 112,6,126,96,7,56,224,7,56,224,1,56,128,0,153,0,0,90,0,0,56,0
630 DATA 0,56,0,0,0,0,0,0,0,0,126,0,0,42,0,0,84,0,0,40,0,0
640 DATA 0,0,0,0,102,0,0,231,0,0,195,0,1,129,128,1,129,128,1,129,128
650 DATA 1,129,128,0,195,0,0,195,0,4,195,32,2,102,64,2,36,64,1,0,128
660 DATA 1,0,128,0,153,0,0,153,0,0,0,0,0,84,0,0,42,0,0,20,0,0
