5 BASE=2*4096 :POKE 53272,PEEK(53272)OR 8 :REM PUT BIT MAP AT 8192
10 POKE 53265,PEEK(53265)OR 32 :REM ENTER BIT MAP MODE
20 FOR I=BASE TO BASE+7999 :POKE I,0 :NEXT :REM CLEAR BIT
30 FOR I=1024 TO 2023 :POKE I,3 :NEXT :REM SET COLOR TO CYAN AND BLACK
50 FOR X=0 TO 319 STEP.5 :REM WAVE WILL FILL THE SCREEN
60 Y=INT(90+80*SIN(X/10))
70 CH=INT(X/8)
80 RO=INT(Y/8)
85 LN=Y AND 7
90 BY=BASE+RO*320+8*CH+LN
100 BI=7-(XAND7)
110 POKE BY,PEEK(BY)OR(2^BI)
120 NEXT X
125 POKE 1024,16
130 GOTO 130
